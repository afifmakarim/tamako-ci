<?php defined('BASEPATH') OR exit('No direct script access allowed');


class DB_M extends CI_Model {

  function __construct(){

    parent::__construct();
    $this->load->database(); 

  }

  public function logEvents($signature, $body, $messageSource, $eventType, $message, $displayName, $uuid)
  {
    $this->db->set('signature', $signature)
         ->set('events', $body)
         ->set('Source', $messageSource)
         ->set('type', $eventType)
         ->set('userMessage', $message)
         ->set('displayName', $displayName)
         ->set('userId', $uuid)
         ->insert('tamako_eventlogs');
    return $this->db->insert_id();
  }

  public function add_memer($data) {
    return $this->db->insert('tamako_bikinMeme', $data);
  }

  public function check_add_meme($userId, $linkGambar){
    $query = $this->db->get_where('tamako_bikinMeme', array('userId' => $userId));
    $dbs = $query->row_array();
    if ($dbs > 0) {

        $data = array(
                'linkGambar' => $linkGambar
        );
        
        $this->db->where('userId', $userId);
        $this->db->update('tamako_bikinMeme', $data);
        return true;
        }

        return false;
    }

    public function get_meme($userId) {
        $query = $this->db->select('linkGambar')
                            ->where('userId', $userId)
                            ->get('tamako_bikinMeme');
        return $query->row_array();
    }

    public function cancel_meme($userId) {
        $query = $this->db->get_where('tamako_bikinMeme', array('userId' => $userId));
        $dbs = $query->row_array();
        if ($dbs > 0) {
            $this->db->where('userId', $userId);
            $this->db->delete('tamako_bikinMeme');
            return true;
        }
        return false;
       
    }

    public function add_recognition($data)
    {   
        $query = $this->db->get_where('tamako_facerecognition', array('groupId' => $data['groupId']));
        $dbs = $query->row_array();
        if ($dbs > 0) {
            return false;
        }
        $this->db->insert('tamako_facerecognition', $data);
        return true;
    }

    public function check_recognition($groupId)
    {
        $query = $this->db->get_where('tamako_facerecognition', array('groupId' => $groupId));
        $dbs = $query->row_array();
        if ($dbs > 0) {
            return true;
        }
        return false;
    }

    public function delete_recognition($groupId)
    {
        $query = $this->db->get_where('tamako_facerecognition', array('groupId' => $groupId));
        $dbs = $query->row_array();
        if ($dbs > 0) {
            $this->db->where('groupId', $groupId);
            $this->db->delete('tamako_facerecognition');
            return true;
        }
        return false;
    }

}