<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once('bot_settings.php');

class Data_M extends CI_Model {

  function __construct(){

    parent::__construct();
    $this->load->database(); 

  }

  public function ping($host, $port, $timeout) { 
    $tB = microtime(true); 
    $fP = fSockOpen($host, $port, $errno, $errstr, $timeout); 
    if (!$fP) { return "down"; } 
    $tA = microtime(true); 
    return round((($tA - $tB) * 1000), 0)." ms"; 
  }

  public function api_content($id)
  {
      $url = 'https://api.line.me/v2/bot/message/' . $id . '/content';

      // リクエストを生成
      $headers = ['Content-Type: application/json',
          'Authorization: Bearer ' . LINE_MESSAGE_ACCESS_TOKEN];

      // curlのオプション
      $options = [CURLOPT_URL => $url,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_HTTPHEADER => $headers];
      // curlセッションの実行
      $curl = curl_init();
      curl_setopt_array($curl, $options);
      $response = curl_exec($curl);
      curl_close($curl);

      return $response;

  }

  public function curl_api($url, $method = 'GET', $_header_show = null, $header_param = null)
  {
      $header_params[] = $header_param;

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
      if ($_header_show) curl_setopt($ch, CURLOPT_HEADER, TRUE);

      //curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
      if ($header_param) curl_setopt($ch, CURLOPT_HTTPHEADER, $header_params);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, FALSE);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
      curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
      curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);

      $output = curl_exec($ch);
      $info = curl_getinfo($ch);
      curl_close($ch);

      return json_decode($output, true);
  }

  public function curl_api_header($url) {
		$opts = array(
      'http'=>array(
      'method'=>"GET",
      'header'=>"User-Agent: lashaparesha api script\r\n"
      ));
      $context = stream_context_create($opts);
      $file = file_get_contents($url, false, $context);
      $decode_json = json_decode($file, true);
      return $decode_json;
  }

  public function curl_mashape($url) {
    $opts = array(
      'http'=>array(
        'method'=>"GET",
        'header'=>"X-Mashape-Key: fwx7jEDlMlmshhIVORiiHcodhetxp1Tv9vsjsn0ZMUT0CXNAfp"               
          )
        );
    
      $context = stream_context_create($opts);
      $res = file_get_contents($url, false, $context);
      $json = json_decode($res, true);
      return $json;
  }

  public function GET_32_BIT ($ID_64) {
      $upper = gmp_mul( bindec("00000001000100000000000000000001") , "4294967296" );
      return gmp_strval(gmp_sub($ID_64,$upper));
  }

  public function flexMessageDota($arrayDota){
    
    $json_flex = '{
      "type": "bubble",
      "hero": {
        "type": "image",
        "url": "'.$arrayDota['displayImage'].'",
        "size": "full",
        "aspectRatio": "20:13",
        "aspectMode": "cover"
      },
      "body": {
        "type": "box",
        "layout": "vertical",
        "contents": [
          {
            "type": "text",
            "text": "'.$arrayDota['persona'].'",
            "weight": "bold",
            "size": "md"
          },
          {
            "type": "box",
            "layout": "vertical",
            "margin": "lg",
            "spacing": "sm",
            "contents": [
              {
                "type": "box",
                "layout": "baseline",
                "spacing": "sm",
                "contents": [
                  {
                    "type": "text",
                    "text": "Win",
                    "color": "#aaaaaa",
                    "size": "sm",
                    "flex": 3
                  },
                  {
                    "type": "text",
                    "text": "'.$arrayDota['win'].'",
                    "wrap": true,
                    "color": "#666666",
                    "size": "sm",
                    "flex": 5
                  }
                ]
              },
              {
                "type": "box",
                "layout": "baseline",
                "spacing": "sm",
                "contents": [
                  {
                    "type": "text",
                    "text": "Total Match",
                    "color": "#aaaaaa",
                    "size": "sm",
                    "flex": 3
                  },
                  {
                    "type": "text",
                    "text": "'.$arrayDota['totalMatch'].'",
                    "wrap": true,
                    "color": "#666666",
                    "size": "sm",
                    "flex": 5
                  }
                ]
              },
              {
                "type": "box",
                "layout": "baseline",
                "spacing": "sm",
                "contents": [
                  {
                    "type": "text",
                    "text": "Signature Hero",
                    "color": "#aaaaaa",
                    "size": "sm",
                    "flex": 3
                  },
                  {
                    "type": "text",
                    "text": "'.$arrayDota['signatureHero'].'",
                    "wrap": true,
                    "color": "#666666",
                    "size": "sm",
                    "flex": 5
                  }
                ]
              }
            ]
          },
          {
            "type": "text",
            "text": "Recent Match Played",
            "weight": "bold",
            "size": "sm",
            "margin": "md"
          },
          {
            "type": "box",
            "layout": "vertical",
            "margin": "lg",
            "spacing": "sm",
            "contents": [
              {
                "type": "box",
                "layout": "baseline",
                "spacing": "sm",
                "contents": [
                  {
                    "type": "text",
                    "text": "Hero",
                    "color": "#aaaaaa",
                    "size": "sm",
                    "flex": 3
                  },
                  {
                    "type": "text",
                    "text": "'.$arrayDota['lastMatchHero'].'",
                    "wrap": true,
                    "color": "#666666",
                    "size": "sm",
                    "flex": 5
                  }
                ]
              },
              {
                "type": "box",
                "layout": "baseline",
                "spacing": "sm",
                "contents": [
                  {
                    "type": "text",
                    "text": "K/D/A",
                    "color": "#aaaaaa",
                    "size": "sm",
                    "flex": 3
                  },
                  {
                    "type": "text",
                    "text": "'.$arrayDota['kda'].'",
                    "wrap": true,
                    "color": "#666666",
                    "size": "sm",
                    "flex": 5
                  }
                ]
              },
              {
                "type": "box",
                "layout": "baseline",
                "spacing": "sm",
                "contents": [
                  {
                    "type": "text",
                    "text": "LH/GPM",
                    "color": "#aaaaaa",
                    "size": "sm",
                    "flex": 3
                  },
                  {
                    "type": "text",
                    "text": "'.$arrayDota['lh'].'",
                    "wrap": true,
                    "color": "#666666",
                    "size": "sm",
                    "flex": 5
                  }
                ]
              }
            ]
          }
        ]
      },
      "footer": {
        "type": "box",
        "layout": "vertical",
        "spacing": "sm",
        "contents": [
          {
            "type": "button",
            "style": "primary",
            "color": "#1b2838",
            "height": "sm",
            "action": {
              "type": "uri",
              "label": "Open Steam",
              "uri": "'.$arrayDota['url_steam'].'"
            }
          },
          {
            "type": "button",
            "style": "link",
            "height": "sm",
            "action": {
              "type": "uri",
              "label": "Open Dotabuff",
              "uri": "'.$arrayDota['url_dotabuff'].'"
            }
          },
          {
            "type": "spacer",
            "size": "sm"
          }
        ],
        "flex": 0
      }
    }';

    return $json_flex;
  }
  
  public function id_to_HeroName($code){

      $code = strtoupper($code);
  
      $heroList = array(
          '1' => 'Anti-Mage',
          '2' => 'Axe',
          '3' => 'Bane',
          '4' => 'Bloodseeker',
          '5' => 'Crystal Maiden',
          '6' => 'Drow',
          '7' => 'Earthshaker',
          '8' => 'Juggernaut',
          '9' => 'Mirana',
          '10' => 'Morphling',
          '11' => 'Shadow Fiend',
          '12' => 'Phantom Lancer',
          '13' => 'Puck',
          '14' => 'Pudge',
          '15' => 'Razor',
          '16' => 'Sand King',
          '17' => 'Storm Spirit',
          '18' => 'Sven',
          '19' => 'Tiny',
          '20' => 'Vengeful Spirit',
          '21' => 'Windranger',
          '22' => 'Zeus',
          '23' => 'Kunkka',
          '24' => 'Benin',
          '25' => 'Lina',
          '26' => 'Lion',
          '27' => 'Shadow Shaman',
          '28' => 'Slardar',
          '29' => 'Tidehunter',
          '30' => 'Witch Doctor',
          '31' => 'Lich',
          '32' => 'Riki',
          '33' => 'Enigma',
          '34' => 'Tinker',
          '35' => 'Sniper',
          '36' => 'Necrophos',
          '37' => 'Warlock',
          '38' => 'Beastmaster',
          '39' => 'Queen of Pain',
          '40' => 'Venomancer',
          '41' => 'Faceless Void',
          '42' => 'Wraith King',
          '43' => 'Death Prophet',
          '44' => 'Phantom Assasin',
          '45' => 'Pugna',
          '46' => 'Templar Assasin',
          '47' => 'Viper',
          '48' => 'Luna',
          '49' => 'Dragon Knight',
          '50' => 'Dazzle',
          '51' => 'Clockwerk',
          '52' => 'Leshrac',
          '53' => 'Nature Prohet',
          '54' => 'Lifestealer',
          '55' => 'Dark Seer',
          '56' => 'Clinkz',
          '57' => 'Omniknight',
          '58' => 'Enchantress',
          '59' => 'Huskar',
          '60' => 'Night Stalker',
          '61' => 'Broodmother',
          '62' => 'Bounty Hunter',
          '63' => 'Weaver',
          '64' => 'Jakiro',
          '65' => 'Batrider',
          '66' => 'Chen',
          '67' => 'Spectre',
          '68' => 'Ancient Apparition',
          '69' => 'Doom',
          '70' => 'Ursa',
          '71' => 'Spirit Breaker',
          '72' => 'Gyrocopter',
          '73' => 'Alchemist',
          '74' => 'Invoker',
          '75' => 'Silencer',
          '76' => 'Outworld Devourer',
          '77' => 'Lycan',
          '78' => 'Brewmaster',
          '79' => 'Shadow Demon',
          '80' => 'Lone Druid',
          '81' => 'Chaos Knight',
          '82' => 'Meepo',
          '83' => 'Treant Protector',
          '84' => 'Ogre Magi',
          '85' => 'Undying',
          '86' => 'Rubick',
          '87' => 'Disruptor',
          '88' => 'Nyx Assasin',
          '89' => 'Naga Siren',
          '90' => 'Keeper of the Light',
          '91' => 'Io',
          '92' => 'Visage',
          '93' => 'Slark',
          '94' => 'Medusa',
          '95' => 'Troll Warlord',
          '96' => 'Centaur Warrunner',
          '97' => 'Magnus',
          '98' => 'Timbersaw',
          '99' => 'Bristleback',
          '100' => 'Tusk',
          '101' => 'Skywrath Mage',
          '102' => 'Abaddon',
          '103' => 'Elder Titan',
          '104' => 'Legion Commander',
          '105' => 'Techies',
          '106' => 'Ember Spirit',
          '107' => 'Earth Spirit',
          '108' => 'Underlord',
          '109' => 'Terrorblade',
          '110' => 'Phoenix',
          '111' => 'Oracle',
          '112' => 'Winter Wyvern',
          '113' => 'Arc Warden',
          '114' => 'Monkey King',
          '119' => 'Dark Willow',
          '120' => 'Pangolier'
      );
  
      if( !$heroList[$code] ) return $code;
      else return $heroList[$code];
      }

    public function flexMessageOsu($array){
      
      $json = '{
        "type": "carousel",
        "contents": [
          {
          "type": "bubble",
          "body": {
            "type": "box",
            "layout": "vertical",
            "contents": [
            {
              "type": "text",
              "text": "osu!",
              "weight": "bold",
              "color": "#dc98a4",
              "size": "sm"
            },
            {
              "type": "text",
              "text": "'.$array['mania']['username'].'",
              "weight": "bold",
              "size": "xxl",
              "margin": "md"
            },
            {
              "type": "text",
              "text": "'.$array['mania']['country'].'",
              "size": "xs",
              "color": "#aaaaaa",
              "wrap": true
            },
            {
              "type": "separator",
              "margin": "xxl"
            },
            {
              "type": "box",
              "layout": "vertical",
              "margin": "xxl",
              "spacing": "sm",
              "contents": [
              {
                "type": "box",
                "layout": "horizontal",
                "contents": [
                {
                  "type": "text",
                  "text": "Standard",
                  "size": "md",
                  "color": "#555555",
                  "flex": 0,
                  "weight": "bold"
                }
                ]
              },
              {
                "type": "box",
                "layout": "horizontal",
                "contents": [
                {
                  "type": "text",
                  "text": "Country Rank",
                  "size": "sm",
                  "color": "#555555",
                  "flex": 0
                },
                {
                  "type": "text",
                  "text": "'.$array['std_api']['countryRank'].'",
                  "size": "sm",
                  "color": "#111111",
                  "align": "end"
                }
                ]
              },
              {
                "type": "box",
                "layout": "horizontal",
                "contents": [
                {
                  "type": "text",
                  "text": "Global Rank",
                  "size": "sm",
                  "color": "#555555",
                  "flex": 0
                },
                {
                  "type": "text",
                  "text": "'.$array['std_api']['globalRank'].'",
                  "size": "sm",
                  "color": "#111111",
                  "align": "end"
                }
                ]
              },
              {
                "type": "box",
                "layout": "horizontal",
                "contents": [
                {
                  "type": "text",
                  "text": "Accuracy",
                  "size": "sm",
                  "color": "#555555",
                  "flex": 0
                },
                {
                  "type": "text",
                  "text": "'.round($array['std_api']['accuracy'],2).'",
                  "size": "sm",
                  "color": "#111111",
                  "align": "end"
                }
                ]
              },
              {
                "type": "separator",
                "margin": "xxl"
              },
              {
                "type": "box",
                "layout": "horizontal",
                "contents": [
                {
                  "type": "text",
                  "text": "Mania",
                  "size": "md",
                  "color": "#555555",
                  "flex": 0,
                  "weight": "bold"
                }
                ],
                "margin": "xxl"
              },
              {
                "type": "box",
                "layout": "horizontal",
                "contents": [
                {
                  "type": "text",
                  "text": "Country Rank",
                  "size": "sm",
                  "color": "#555555"
                },
                {
                  "type": "text",
                  "text": "'.$array['mania']['countryRank'].'",
                  "size": "sm",
                  "color": "#111111",
                  "align": "end"
                }
                ]
              },
              {
                "type": "box",
                "layout": "horizontal",
                "contents": [
                {
                  "type": "text",
                  "text": "Global Rank",
                  "size": "sm",
                  "color": "#555555"
                },
                {
                  "type": "text",
                  "text": "'.$array['mania']['globalRank'].'",
                  "size": "sm",
                  "color": "#111111",
                  "align": "end"
                }
                ]
              },
              {
                "type": "box",
                "layout": "horizontal",
                "contents": [
                {
                  "type": "text",
                  "text": "Accuracy",
                  "size": "sm",
                  "color": "#555555"
                },
                {
                  "type": "text",
                  "text": "'.round($array['mania']['accuracy'],2).'",
                  "size": "sm",
                  "color": "#111111",
                  "align": "end"
                }
                ]
              }
              ]
            },
            {
              "type": "separator",
              "margin": "xxl"
            },
            {
              "type": "box",
              "layout": "horizontal",
              "margin": "md",
              "contents": [
              {
                "type": "text",
                "text": "Join Date",
                "size": "xs",
                "color": "#aaaaaa",
                "flex": 0
              },
              {
                "type": "text",
                "text": "'.$array['mania']['join_date'].'",
                "color": "#aaaaaa",
                "size": "xs",
                "align": "end"
              }
              ]
            }
            ]
          },
          "styles": {
            "footer": {
            "separator": true
            }
          }
          },
          {
          "type": "bubble",
          "body": {
            "type": "box",
            "layout": "vertical",
            "contents": [{
            "type": "box",
            "layout": "vertical",
            "contents": [
              {
              "type": "image",
              "url": "'.$array['mania']['pp'].'"
              }
            ]
            },
            {
              "type": "separator",
              "margin": "xxl"
            },
            {
              "type": "box",
              "layout": "vertical",
              "margin": "xxl",
              "spacing": "sm",
              "contents": [
              {
                "type": "box",
                "layout": "horizontal",
                "contents": [
                {
                  "type": "text",
                  "text": "Taiko",
                  "size": "md",
                  "color": "#555555",
                  "flex": 0,
                  "weight": "bold"
                }
                ]
              },
              {
                "type": "box",
                "layout": "horizontal",
                "contents": [
                {
                  "type": "text",
                  "text": "Country Rank",
                  "size": "sm",
                  "color": "#555555",
                  "flex": 0
                },
                {
                  "type": "text",
                  "text": "'.$array['taiko_api']['countryRank'].'",
                  "size": "sm",
                  "color": "#111111",
                  "align": "end"
                }
                ]
              },
              {
                "type": "box",
                "layout": "horizontal",
                "contents": [
                {
                  "type": "text",
                  "text": "Global Rank",
                  "size": "sm",
                  "color": "#555555",
                  "flex": 0
                },
                {
                  "type": "text",
                  "text": "'.$array['taiko_api']['globalRank'].'",
                  "size": "sm",
                  "color": "#111111",
                  "align": "end"
                }
                ]
              },
              {
                "type": "box",
                "layout": "horizontal",
                "contents": [
                {
                  "type": "text",
                  "text": "Accuracy",
                  "size": "sm",
                  "color": "#555555",
                  "flex": 0
                },
                {
                  "type": "text",
                  "text": "'.round($array['taiko_api']['accuracy'],2).'",
                  "size": "sm",
                  "color": "#111111",
                  "align": "end"
                }
                ]
              },
              {
                "type": "separator",
                "margin": "xxl"
              },
              {
                "type": "box",
                "layout": "horizontal",
                "contents": [
                {
                  "type": "text",
                  "text": "Catch the beat",
                  "size": "md",
                  "color": "#555555",
                  "flex": 0,
                  "weight": "bold"
                }
                ],
                "margin": "xxl"
              },
              {
                "type": "box",
                "layout": "horizontal",
                "contents": [
                {
                  "type": "text",
                  "text": "Country Rank",
                  "size": "sm",
                  "color": "#555555"
                },
                {
                  "type": "text",
                  "text": "'.$array['ctb_api']['countryRank'].'",
                  "size": "sm",
                  "color": "#111111",
                  "align": "end"
                }
                ]
              },
              {
                "type": "box",
                "layout": "horizontal",
                "contents": [
                {
                  "type": "text",
                  "text": "Global Rank",
                  "size": "sm",
                  "color": "#555555"
                },
                {
                  "type": "text",
                  "text": "'.$array['ctb_api']['globalRank'].'",
                  "size": "sm",
                  "color": "#111111",
                  "align": "end"
                }
                ]
              },
              {
                "type": "box",
                "layout": "horizontal",
                "contents": [
                {
                  "type": "text",
                  "text": "Accuracy",
                  "size": "sm",
                  "color": "#555555"
                },
                {
                  "type": "text",
                  "text": "'.round($array['ctb_api']['accuracy'],2).'",
                  "size": "sm",
                  "color": "#111111",
                  "align": "end"
                }
                ]
              }
              ]
            },
            {
              "type": "separator",
              "margin": "xxl"
            }
            ]
          },
          "styles": {
            "footer": {
            "separator": true
            }
          }
          }
        ]
        }';

        return $json;
    }

    public function default_value($message)
    {
      if (strlen($message) == 0 ){
        return "-";
      }
      return $message;
    }

    private function default_json($message)
    {
        if (strlen($message) > 0) {
          return $message;
        }
        $json_content = '{
          "type": "box",
          "layout": "baseline",
          "contents": [
            {
            "type": "text",
            "text": "-",
            "size": "xs",
            "color": "#8c8c8c",
            "margin": "md",
            "flex": 1,
            "wrap": true
            },
            {
            "type": "text",
            "text": "-",
            "flex": 0,
            "margin": "md",
            "size": "xs",
            "color": "#8c8c8c"
            }
          ]
          }';
        return $json_content;
    }
    public function steam_state( $code ){

      $code = strtoupper($code);
  
      $countryList = array(
          '0' => 'Offline',
          '1' => 'Online',
          '2' => 'Busy',
          '3' => 'Away',
          '4' => 'Snooze',
          '5' => 'Looking to trade',
          '6' => 'Looking to play',
      );

      if( !$countryList[$code] ) return $code;
      else return $countryList[$code];
    
    }
    public function code_to_country( $code ){

      $code = strtoupper($code);
  
      $countryList = array(
          'AF' => 'Afghanistan',
          'AX' => 'Aland Islands',
          'AL' => 'Albania',
          'DZ' => 'Algeria',
          'AS' => 'American Samoa',
          'AD' => 'Andorra',
          'AO' => 'Angola',
          'AI' => 'Anguilla',
          'AQ' => 'Antarctica',
          'AG' => 'Antigua and Barbuda',
          'AR' => 'Argentina',
          'AM' => 'Armenia',
          'AW' => 'Aruba',
          'AU' => 'Australia',
          'AT' => 'Austria',
          'AZ' => 'Azerbaijan',
          'BS' => 'Bahamas the',
          'BH' => 'Bahrain',
          'BD' => 'Bangladesh',
          'BB' => 'Barbados',
          'BY' => 'Belarus',
          'BE' => 'Belgium',
          'BZ' => 'Belize',
          'BJ' => 'Benin',
          'BM' => 'Bermuda',
          'BT' => 'Bhutan',
          'BO' => 'Bolivia',
          'BA' => 'Bosnia and Herzegovina',
          'BW' => 'Botswana',
          'BV' => 'Bouvet Island (Bouvetoya)',
          'BR' => 'Brazil',
          'IO' => 'British Indian Ocean Territory (Chagos Archipelago)',
          'VG' => 'British Virgin Islands',
          'BN' => 'Brunei Darussalam',
          'BG' => 'Bulgaria',
          'BF' => 'Burkina Faso',
          'BI' => 'Burundi',
          'KH' => 'Cambodia',
          'CM' => 'Cameroon',
          'CA' => 'Canada',
          'CV' => 'Cape Verde',
          'KY' => 'Cayman Islands',
          'CF' => 'Central African Republic',
          'TD' => 'Chad',
          'CL' => 'Chile',
          'CN' => 'China',
          'CX' => 'Christmas Island',
          'CC' => 'Cocos (Keeling) Islands',
          'CO' => 'Colombia',
          'KM' => 'Comoros the',
          'CD' => 'Congo',
          'CG' => 'Congo the',
          'CK' => 'Cook Islands',
          'CR' => 'Costa Rica',
          'CI' => 'Cote d\'Ivoire',
          'HR' => 'Croatia',
          'CU' => 'Cuba',
          'CY' => 'Cyprus',
          'CZ' => 'Czech Republic',
          'DK' => 'Denmark',
          'DJ' => 'Djibouti',
          'DM' => 'Dominica',
          'DO' => 'Dominican Republic',
          'EC' => 'Ecuador',
          'EG' => 'Egypt',
          'SV' => 'El Salvador',
          'GQ' => 'Equatorial Guinea',
          'ER' => 'Eritrea',
          'EE' => 'Estonia',
          'ET' => 'Ethiopia',
          'FO' => 'Faroe Islands',
          'FK' => 'Falkland Islands (Malvinas)',
          'FJ' => 'Fiji the Fiji Islands',
          'FI' => 'Finland',
          'FR' => 'France, French Republic',
          'GF' => 'French Guiana',
          'PF' => 'French Polynesia',
          'TF' => 'French Southern Territories',
          'GA' => 'Gabon',
          'GM' => 'Gambia the',
          'GE' => 'Georgia',
          'DE' => 'Germany',
          'GH' => 'Ghana',
          'GI' => 'Gibraltar',
          'GR' => 'Greece',
          'GL' => 'Greenland',
          'GD' => 'Grenada',
          'GP' => 'Guadeloupe',
          'GU' => 'Guam',
          'GT' => 'Guatemala',
          'GG' => 'Guernsey',
          'GN' => 'Guinea',
          'GW' => 'Guinea-Bissau',
          'GY' => 'Guyana',
          'HT' => 'Haiti',
          'HM' => 'Heard Island and McDonald Islands',
          'VA' => 'Holy See (Vatican City State)',
          'HN' => 'Honduras',
          'HK' => 'Hong Kong',
          'HU' => 'Hungary',
          'IS' => 'Iceland',
          'IN' => 'India',
          'ID' => 'Indonesia',
          'IR' => 'Iran',
          'IQ' => 'Iraq',
          'IE' => 'Ireland',
          'IM' => 'Isle of Man',
          'IL' => 'Israel',
          'IT' => 'Italy',
          'JM' => 'Jamaica',
          'JP' => 'Japan',
          'JE' => 'Jersey',
          'JO' => 'Jordan',
          'KZ' => 'Kazakhstan',
          'KE' => 'Kenya',
          'KI' => 'Kiribati',
          'KP' => 'Korea',
          'KR' => 'Korea',
          'KW' => 'Kuwait',
          'KG' => 'Kyrgyz Republic',
          'LA' => 'Lao',
          'LV' => 'Latvia',
          'LB' => 'Lebanon',
          'LS' => 'Lesotho',
          'LR' => 'Liberia',
          'LY' => 'Libyan Arab Jamahiriya',
          'LI' => 'Liechtenstein',
          'LT' => 'Lithuania',
          'LU' => 'Luxembourg',
          'MO' => 'Macao',
          'MK' => 'Macedonia',
          'MG' => 'Madagascar',
          'MW' => 'Malawi',
          'MY' => 'Malaysia',
          'MV' => 'Maldives',
          'ML' => 'Mali',
          'MT' => 'Malta',
          'MH' => 'Marshall Islands',
          'MQ' => 'Martinique',
          'MR' => 'Mauritania',
          'MU' => 'Mauritius',
          'YT' => 'Mayotte',
          'MX' => 'Mexico',
          'FM' => 'Micronesia',
          'MD' => 'Moldova',
          'MC' => 'Monaco',
          'MN' => 'Mongolia',
          'ME' => 'Montenegro',
          'MS' => 'Montserrat',
          'MA' => 'Morocco',
          'MZ' => 'Mozambique',
          'MM' => 'Myanmar',
          'NA' => 'Namibia',
          'NR' => 'Nauru',
          'NP' => 'Nepal',
          'AN' => 'Netherlands Antilles',
          'NL' => 'Netherlands the',
          'NC' => 'New Caledonia',
          'NZ' => 'New Zealand',
          'NI' => 'Nicaragua',
          'NE' => 'Niger',
          'NG' => 'Nigeria',
          'NU' => 'Niue',
          'NF' => 'Norfolk Island',
          'MP' => 'Northern Mariana Islands',
          'NO' => 'Norway',
          'OM' => 'Oman',
          'PK' => 'Pakistan',
          'PW' => 'Palau',
          'PS' => 'Palestinian Territory',
          'PA' => 'Panama',
          'PG' => 'Papua New Guinea',
          'PY' => 'Paraguay',
          'PE' => 'Peru',
          'PH' => 'Philippines',
          'PN' => 'Pitcairn Islands',
          'PL' => 'Poland',
          'PT' => 'Portugal, Portuguese Republic',
          'PR' => 'Puerto Rico',
          'QA' => 'Qatar',
          'RE' => 'Reunion',
          'RO' => 'Romania',
          'RU' => 'Russian Federation',
          'RW' => 'Rwanda',
          'BL' => 'Saint Barthelemy',
          'SH' => 'Saint Helena',
          'KN' => 'Saint Kitts and Nevis',
          'LC' => 'Saint Lucia',
          'MF' => 'Saint Martin',
          'PM' => 'Saint Pierre and Miquelon',
          'VC' => 'Saint Vincent and the Grenadines',
          'WS' => 'Samoa',
          'SM' => 'San Marino',
          'ST' => 'Sao Tome and Principe',
          'SA' => 'Saudi Arabia',
          'SN' => 'Senegal',
          'RS' => 'Serbia',
          'SC' => 'Seychelles',
          'SL' => 'Sierra Leone',
          'SG' => 'Singapore',
          'SK' => 'Slovakia (Slovak Republic)',
          'SI' => 'Slovenia',
          'SB' => 'Solomon Islands',
          'SO' => 'Somalia, Somali Republic',
          'ZA' => 'South Africa',
          'GS' => 'South Georgia and the South Sandwich Islands',
          'ES' => 'Spain',
          'LK' => 'Sri Lanka',
          'SD' => 'Sudan',
          'SR' => 'Suriname',
          'SJ' => 'Svalbard & Jan Mayen Islands',
          'SZ' => 'Swaziland',
          'SE' => 'Sweden',
          'CH' => 'Switzerland, Swiss Confederation',
          'SY' => 'Syrian Arab Republic',
          'TW' => 'Taiwan',
          'TJ' => 'Tajikistan',
          'TZ' => 'Tanzania',
          'TH' => 'Thailand',
          'TL' => 'Timor-Leste',
          'TG' => 'Togo',
          'TK' => 'Tokelau',
          'TO' => 'Tonga',
          'TT' => 'Trinidad and Tobago',
          'TN' => 'Tunisia',
          'TR' => 'Turkey',
          'TM' => 'Turkmenistan',
          'TC' => 'Turks and Caicos Islands',
          'TV' => 'Tuvalu',
          'UG' => 'Uganda',
          'UA' => 'Ukraine',
          'AE' => 'United Arab Emirates',
          'GB' => 'United Kingdom',
          'US' => 'United States of America',
          'UM' => 'United States Minor Outlying Islands',
          'VI' => 'United States Virgin Islands',
          'UY' => 'Uruguay, Eastern Republic of',
          'UZ' => 'Uzbekistan',
          'VU' => 'Vanuatu',
          'VE' => 'Venezuela',
          'VN' => 'Vietnam',
          'WF' => 'Wallis and Futuna',
          'EH' => 'Western Sahara',
          'YE' => 'Yemen',
          'ZM' => 'Zambia',
          'ZW' => 'Zimbabwe'
      );
  
      if( !$countryList[$code] ) return $code;
      else return $countryList[$code];
      }

      public function flexMessageSteam($arrayProfile, $recent_game_name, $recent_game_hours)
      {
        // if (count($recent_game_name) == 0) {
        //   $json_content = '{
        //     "type": "box",
        //     "layout": "baseline",
        //     "contents": [
        //       {
        //       "type": "text",
        //       "text": "-",
        //       "size": "xs",
        //       "color": "#8c8c8c",
        //       "margin": "md",
        //       "flex": 1,
        //       "wrap": true
        //       },
        //       {
        //       "type": "text",
        //       "text": "-",
        //       "flex": 0,
        //       "margin": "md",
        //       "size": "xs",
        //       "color": "#8c8c8c"
        //       }
        //     ]
        //     }';
        // } else {
          if (is_array($recent_game_name) == 1) {
          for($i = 0; $i < count($recent_game_name); $i++) {
            $json_content[] = '{
              "type": "box",
              "layout": "baseline",
              "contents": [
                {
                  "type": "text",
                  "text": "'.$recent_game_name[$i].'",
                  "size": "xs",
                  "color": "#8c8c8c",
                  "margin": "md",
                  "flex": 1,
                  "wrap": true
                },
                {
                  "type": "text",
                  "text": "'.$recent_game_hours[$i].' hours",
                  "flex": 0,
                  "margin": "md",
                  "size": "xs",
                  "color": "#8c8c8c"
                }
              ]
            }';
          }
        } else {
          $json_content[] = '{
            "type": "box",
            "layout": "baseline",
            "contents": [
              {
                "type": "text",
                "text": "-",
                "size": "xs",
                "color": "#8c8c8c",
                "margin": "md",
                "flex": 1,
                "wrap": true
              },
              {
                "type": "text",
                "text": "-",
                "flex": 0,
                "margin": "md",
                "size": "xs",
                "color": "#8c8c8c"
              }
            ]
          }';
        }

        $join_json = implode(', ', $json_content);

        $json_flex = '{
          "type": "carousel",
          "contents": [
            {
            "type": "bubble",
            "size": "kilo",
            "hero": {
              "type": "image",
              "url": "'.$arrayProfile['avatarfull'].'",
              "size": "full",
              "aspectMode": "cover",
              "aspectRatio": "1:1"
            },
            "body": {
              "type": "box",
              "layout": "vertical",
              "contents": [
              {
                "type": "text",
                "text": "'.$arrayProfile['personaname'].'",
                "weight": "bold",
                "size": "md",
                "wrap": true
              },
              {
                "type": "box",
                "layout": "baseline",
                "contents": [
                {
                  "type": "text",
                  "text": "'.$arrayProfile['realname'].'",
                  "size": "xs",
                  "color": "#8c8c8c",
                  "margin": "sm",
                  "flex": 0
                }
                ]
              },
              {
                "type": "box",
                "layout": "baseline",
                "contents": [
                {
                  "type": "text",
                  "text": "'.$arrayProfile['profilestate'].'",
                  "size": "xs",
                  "color": "#8c8c8c",
                  "margin": "md",
                  "flex": 0
                }
                ]
              },
              {
                "type": "box",
                "layout": "vertical",
                "contents": [
                {
                  "type": "spacer"
                },
                {
                  "type": "box",
                  "layout": "baseline",
                  "spacing": "sm",
                  "contents": [
                  {
                    "type": "text",
                    "text": "Recently Played Games",
                    "wrap": true,
                    "size": "xs",
                    "flex": 5,
                    "weight": "bold"
                  }
                  ]
                },
                 '.$join_json.'
                ]
              }
              ],
              "spacing": "sm",
              "paddingAll": "13px"
            },
            "footer": {
              "type": "box",
              "layout": "vertical",
              "contents": [
              {
                "type": "button",
                "action": {
                "type": "uri",
                "label": "Open Steam",
                "uri": "'.$arrayProfile['profile_url'].'"
                },
                "style": "primary",
                "color": "#1b2838"
              }
              ]
            }
            }
          ]
          }';

          return $json_flex;
      }


        public function time2str($ts) {
          if(!ctype_digit($ts))
              $ts = strtotime($ts);

          $diff = time() - $ts;
          if($diff == 0)
              return 'now';
          elseif($diff > 0)
          {
              $day_diff = floor($diff / 86400);
              if($day_diff == 0)
              {
                  if($diff < 60) return 'just now';
                  if($diff < 120) return '1 minute ago';
                  if($diff < 3600) return floor($diff / 60) . ' minutes ago';
                  if($diff < 7200) return '1 hour ago';
                  if($diff < 86400) return floor($diff / 3600) . ' hours ago';
              }
              if($day_diff == 1) return 'Yesterday';
              if($day_diff < 7) return $day_diff . ' days ago';
              if($day_diff < 31) return ceil($day_diff / 7) . ' weeks ago';
              if($day_diff < 60) return 'last month';
              return date('F Y', $ts);
          }
          else
          {
              $diff = abs($diff);
              $day_diff = floor($diff / 86400);
              if($day_diff == 0)
              {
                  if($diff < 120) return 'in a minute';
                  if($diff < 3600) return 'in ' . floor($diff / 60) . ' minutes';
                  if($diff < 7200) return 'in an hour';
                  if($diff < 86400) return 'in ' . floor($diff / 3600) . ' hours';
              }
              if($day_diff == 1) return 'Tomorrow';
              if($day_diff < 4) return date('l', $ts);
              if($day_diff < 7 + (7 - date('w'))) return 'next week';
              if(ceil($day_diff / 7) < 4) return 'in ' . ceil($day_diff / 7) . ' weeks';
              if(date('n', $ts) == date('n') + 1) return 'next month';
              return date('F Y', $ts);
          }
        }

      //USAGE
      public function usage_model($userMessage)
      {
         if(strtolower($userMessage) == 'sing'){
        $message = "Type:\n!sing\nto show song list from Tamako Kitashirakawa (CV: Suzaki Aya)";
        return $message;
        }else if(strtolower($userMessage) == 'about'){
        $message = "Type:\n!about\nto show information about developer";
        return $message;
        }else if(strtolower($userMessage) == 'games'){
        $message = "Type:\n!games [games title]\nto show information about game you mentioned";
        return $message;
        }else if(strtolower($userMessage) == 'manga'){
        $message = "type: !manga [MangaTitle]\nto find the desired manga information";
        return $message;
        }else if(strtolower($userMessage) == 'motw'){
        $message = "type: !motw\nto see 10 top music of the week based on itunes indonesia";
        return $message;
        }else if(strtolower($userMessage) == 'ynm'){
        $message = "!ynm [statement yes no maybe]\n\nEx: !ynm am i handsome?";
        return $message;
        }else if(strtolower($userMessage) == 'chs'){
        $message = "!chs [choice1]-[choice2]-[choice...]\n\nEx: !chs kuliah-kerja-ternak lele";
        return $message;
        }else if(strtolower($userMessage) == 'osu'){
        $message = "!osu <osu username>\nTo show your osu stats";
        return $message;
        }else if(strtolower($userMessage) == 'steam'){
        $message = "!steam <steam username>\n-to show your steam profile\n\nPlease make sure your steam account privacy is on public ^_^";
        return $message;
        }else if(strtolower($userMessage) == 'dota'){
        $message = "!dota <steamID>\n-to show your dota 2 stats and recent game you played \n\nPlease make sure your steam vanity url has been set and check 'expose public match' in dota 2 setting.";
        return $message;
        }else if(strtolower($userMessage) == 'urban'){
        $message = "Type:\n!urban [urban word]\n\nEx: !urban nigga";
        return $message;
        }else if(strtolower($userMessage) == 'lovecalc'){
        $message = "!lovecalc [your name]-[crush name]\n\nEx: !lovecalc tamako-mochizou";
        return $message;
        }else if(strtolower($userMessage) == 'anime'){
        $message = "Type : !anime <anime title> \nto find the desired anime information";
        return $message;
        }else if(strtolower($userMessage) == 'weather'){
        $message = "Type:\n!weather <City Name>\nto show weather report at your city";
        return $message;
        }else if(strtolower($userMessage) == 'stalk'){
        $message = "Type:\n!stalk <instagram username>\nto show 4 recent photos from instagram";
        return $message;
        }else if(strtolower($userMessage) == 'music'){
        $message = "Type:\n!music <artist/singer/band>\nex: !music justin bieber\nto show 5 popular song from your favorite artist that listed on itunes/apple music.";
        return $message;
        }else if(strtolower($userMessage) == 'write'){
        $message = "type : !write [text]\nto overlay your text into image";
        return $message;
        }else if(strtolower($userMessage) == 'meme'){
        $message = "type '!meme' to make meme (1 on 1 chat only)";
        return $message;
        } else {
          $message = "usage '$userMessage' not found";
          return $message;
        }
      }
      //USAGE
}