<?php defined('BASEPATH') OR exit('No direct script access allowed');

//require_once('vendor/autoload.php');
use Phpfastcache\Helper\Psr16Adapter;

class Instagram_M extends CI_Model {

  function __construct(){

    parent::__construct();
    $this->load->database();
    

  }

  public function get_instagram_media($username_ig) {
    Unirest\Request::verifyPeer(false);
    $instagram = new \InstagramScraper\Instagram();
    $medias = $instagram->getMedias($username_ig, 25);
    return $medias;
  }

  public function test(){
    Unirest\Request::verifyPeer(false);
    // If account private you should be subscribed and after auth it will be available
    $instagram = \InstagramScraper\Instagram::withCredentials("test_scrapper_88", "indonesia92", new Psr16Adapter('Files'));
    $instagram->login();
    $instagram->saveSession();
    $media = $instagram->getMediaByUrl("https://www.instagram.com/p/CAWw4sxAlus/");
    $gambar = $media->getImageHighResolutionUrl();
    return $media;
  }

  public function insta_stories($message){
    Unirest\Request::verifyPeer(false);
    // If account private you should be subscribed and after auth it will be available
    $instagram = \InstagramScraper\Instagram::withCredentials("test_scrapper_88", "indonesia92", new Psr16Adapter('Files'));
    $instagram->login();
    $instagram->saveSession();
    $account = $instagram->getMedias($message);
    $id[] = $account[0]->getOwner()->getid();
    $stories = $instagram->getStories($id);
    $slides = $stories[0]->getstories();
    if (isset($slides[0])){
      return $slides;
    } else {
      return null;
    }
  }

}