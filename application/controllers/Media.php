<?php
defined('BASEPATH') or exit('No direct script access allowed');

//require_once('vendor/autoload.php');

//require_once('bot_settings.php');

class Media extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *         http://example.com/index.php/welcome
     *    - or -
     *         http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Data_M');
        $this->load->helper('url');
    }

    public function images($message_id = NULL)
    {
            // $httpClient = new \LINE\LINEBot\HTTPClient\CurlHTTPClient(LINE_MESSAGE_ACCESS_TOKEN);
            // $bot = new \LINE\LINEBot($httpClient, ['channelSecret' => LINE_MESSAGE_CHANNEL_SECRET]);
            // $response = $bot->getMessageContent($message_id);
           
            // if ($response->isSucceeded()) {
            // //$tempfile = tmpfile();
            // //fwrite($tempfile, $response->getRawBody());
            // header("Content-Type: application/octet-stream");
            // echo $response->getRawBody();
            // } else {
            // error_log($response->getHTTPStatus() . ' ' . $response->getRawBody());
            // echo "salah";
            // }
            $response = $this->Data_M->api_content($message_id);
            header("Content-Type: application/octet-stream");
            echo $response;
    }

}