<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
*/

use \LINE\LINEBot;
use \LINE\LINEBot\MessageBuilder\TextMessageBuilder;
use \LINE\LINEBot\MessageBuilder\TemplateBuilder\ButtonTemplateBuilder;
use \LINE\LINEBot\TemplateActionBuilder\MessageTemplateActionBuilder;
use \LINE\LINEBot\TemplateActionBuilder\LocationTemplateActionBuilder;
use \LINE\LINEBot\MessageBuilder\TemplateMessageBuilder;
use \LINE\LINEBot\MessageBuilder\MultiMessageBuilder;
use \LINE\LINEBot\MessageBuilder\AudioMessageBuilder;
use \LINE\LINEBot\MessageBuilder\ImageMessageBuilder;
use \LINE\LINEBot\MessageBuilder\VideoMessageBuilder;
use \LINE\LINEBot\MessageBuilder\StickerMessageBuilder;
use \LINE\LINEBot\TemplateActionBuilder\UriTemplateActionBuilder;
use \LINE\LINEBot\TemplateActionBuilder\PostbackTemplateActionBuilder;
use \LINE\LINEBot\MessageBuilder\TemplateBuilder\CarouselColumnTemplateBuilder;
use \LINE\LINEBot\MessageBuilder\TemplateBuilder\CarouselTemplateBuilder;
use \LINE\LINEBot\MessageBuilder\TemplateBuilder\ImageCarouselColumnTemplateBuilder;
use \LINE\LINEBot\MessageBuilder\TemplateBuilder\ImageCarouselTemplateBuilder;
use \LINE\LINEBot\MessageBuilder\TemplateBuilder\ConfirmTemplateBuilder;

use LINE\LINEBot\MessageBuilder\Flex\ComponentBuilder\BoxComponentBuilder;
use LINE\LINEBot\MessageBuilder\Flex\ComponentBuilder\ButtonComponentBuilder;
use LINE\LINEBot\MessageBuilder\Flex\ComponentBuilder\IconComponentBuilder;
use LINE\LINEBot\MessageBuilder\Flex\ComponentBuilder\ImageComponentBuilder;
use LINE\LINEBot\MessageBuilder\Flex\ComponentBuilder\TextComponentBuilder;
use LINE\LINEBot\MessageBuilder\Flex\ContainerBuilder\CarouselContainerBuilder;
use LINE\LINEBot\MessageBuilder\Flex\ContainerBuilder\BubbleContainerBuilder;
use LINE\LINEBot\MessageBuilder\FlexMessageBuilder;
use LINE\LINEBot\MessageBuilder\RawMessageBuilder;
use LINE\LINEBot\Constant\Flex\ComponentIconSize;
use LINE\LINEBot\Constant\Flex\ComponentImageSize;
use LINE\LINEBot\Constant\Flex\ComponentImageAspectRatio;
use LINE\LINEBot\Constant\Flex\ComponentImageAspectMode;
use LINE\LINEBot\Constant\Flex\ComponentFontSize;
use LINE\LINEBot\Constant\Flex\ComponentFontWeight;
use LINE\LINEBot\Constant\Flex\ComponentMargin;
use LINE\LINEBot\Constant\Flex\ComponentSpacing;
use LINE\LINEBot\Constant\Flex\ComponentButtonStyle;
use LINE\LINEBot\Constant\Flex\ComponentButtonHeight;
use LINE\LINEBot\MessageBuilder\Flex\ComponentBuilder\SpacerComponentBuilder;
use LINE\LINEBot\Constant\Flex\ComponentSpaceSize;
use LINE\LINEBot\Constant\Flex\ComponentGravity;
use LINE\LINEBot\QuickReplyBuilder\ButtonBuilder\QuickReplyButtonBuilder;
use LINE\LINEBot\QuickReplyBuilder\QuickReplyMessageBuilder;
use LINE\LINEBot\MessageBuilder\UrlBuilder;
use LINE\LINEBot\MessageBuilder\ImagemapMessageBuilder;
use LINE\LINEBot\MessageBuilder\Imagemap\BaseSizeBuilder;
use LINE\LINEBot\ImagemapActionBuilder\ImagemapUriActionBuilder;
use LINE\LINEBot\ImagemapActionBuilder\AreaBuilder;
use LINE\LINEBot\ImagemapActionBuilder\ImagemapMessageActionBuilder;
use LINE\LINEBot\Event;
use LINE\LINEBot\Event\BaseEvent;
use LINE\LINEBot\Event\MessageEvent;
use LINE\LINEBot\Event\AccountLinkEvent;
use LINE\LINEBot\Event\MemberJoinEvent; 
use LINE\LINEBot\Event\JoinEvent;
use Phpfastcache\Helper\Psr16Adapter;

// require_once('vendor/autoload.php');
require_once('bot_settings.php');

//require_once("dbconnect.php");


class Webhook extends CI_Controller {

	function __construct() {
        parent::__construct();
		$this->load->model('Data_M');
		$this->load->model('Instagram_M');
		$this->load->model('DB_M');
	}

	function index() {

		if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
			echo "Ready for service!";
			// $ee = $this->Instagram_M->get_instagram_media('9gag');
			// print_r($ee);
			// $ini = $this->Data_M->usage_model("sinxxg");
			// print_r($ini);

			// Unirest\Request::verifyPeer(false);
    		 $instagram = \InstagramScraper\Instagram::withCredentials(new \GuzzleHttp\Client(), "seeder0809", "indonesia92", new Psr16Adapter('Files'));
            $instagram->login(false, true);
            $instagram->saveSession();
    		$media = $instagram->getMediaByUrl("https://www.instagram.com/p/CByHbHsATL-/");
    		$gambar = $media->getImageHighResolutionUrl();
			echo "<pre>";
			print_r($gambar);
			echo "</pre>";
			// $ig_medias = $this->Instagram_M->get_instagram_media("9gag");
			// print_r($ig_medias);
			header('HTTP/1.1 400 Only POST method allowed');
			exit;
		} 
		
        // get request
		$body = file_get_contents('php://input');
		$signature = isset($_SERVER['HTTP_X_LINE_SIGNATURE']) ? $_SERVER['HTTP_X_LINE_SIGNATURE'] : "-";
		$events = json_decode($body, true);

		$httpClient = new \LINE\LINEBot\HTTPClient\CurlHTTPClient(LINE_MESSAGE_ACCESS_TOKEN);
		$bot = new \LINE\LINEBot($httpClient,['channelSecret'=>LINE_MESSAGE_CHANNEL_SECRET]);

		foreach ($events['events'] as $event) {
			if($event['type'] == 'join'){
				$textMessageBuilder = array(new TextMessageBuilder("Hellooo tamako desu '3')/ \ntype '!hello' to see my feature"),
				new TextMessageBuilder("Hai domoo (￣▽￣)ノ	 \ntype '!hello' for keywords (o´ω`o)ﾉ"),
				new TextMessageBuilder("Hello everyone (づ￣ ³￣)づ	 \ntype '!hello' to know me more σ(o'ω'o)"));
				$random_str = $textMessageBuilder[array_rand($textMessageBuilder)];
				$bot->replyMessage($event['replyToken'], $random_str);
			}

			if ($event['type'] == 'memberJoined'){
				$arrayText = ["Selamat datang di Indomaret, selamat berbelanja!","Welcome to the group!", "Hi there! if you need any assistance, i'am always here."];
				shuffle($arrayText);
				$this->textMessage($arrayText[0], $event['replyToken'], $bot);
			}
		} 
		//get reply token and message if events is not null
		if (!is_null($events)) {
			// if ($this->joinEvent->isGroupEvent()) {
			// // if(!is_null($eventJoin)){
			// 	// $arrayTextMessageBuilder = array("Hellooo tamako desu '3')/ \ntype '!hello' to see my feature","Hai domoo (￣▽￣)ノ\ntype '!hello' for keywords (o´ω`o)ﾉ","Hello everyone (づ￣ ³￣)づ\ntype '!hello' to know me more σ(o'ω'o)");
			// 	// shuffle($arrayTextMessageBuilder);
			// 	// $bot->replyMessage($replyToken, $random_str);
			// 	$textMessageBuilder = array(new TextMessageBuilder("Hellooo tamako desu '3')/ \ntype '!hello' to see my feature"),
			// 	new TextMessageBuilder("Hai domoo (￣▽￣)ノ	 \ntype '!hello' for keywords (o´ω`o)ﾉ"),
			// 	new TextMessageBuilder("Hello everyone (づ￣ ³￣)づ	 \ntype '!hello' to know me more σ(o'ω'o)"));
			// 	$random_str = $textMessageBuilder[array_rand($textMessageBuilder)];
			// 	$bot->replyMessage($replyToken, $random_str);
			// }

			$replyToken = $events['events'][0]['replyToken'];
			$messageId = $events['events'][0]['message']['id'];
			$message = $events['events'][0]['message']['text'];
			$messageSource = $events['events'][0]['source']['type'];
			$uuid = $events['events'][0]['source']['userId'];
			$eventType = $events['events'][0]['type'];

			if($messageSource == 'group'){
				$eventId = $events['events'][0]['source']['groupId'];
				$response = $bot->getGroupMemberProfile($event['source']['groupId'], $event['source']['userId']);
				if ($response->isSucceeded()) {
				$profile = $response->getJSONDecodedBody();
				$displayName = $profile['displayName'];
				}
			}if($messageSource == 'room'){
				$eventId = $events['events'][0]['source']['roomId'];
				$response = $bot->getRoomMemberProfile($event['source']['roomId'], $event['source']['userId']);
				if ($response->isSucceeded()) {
				$profile = $response->getJSONDecodedBody();
				$displayName = $profile['displayName'];
				}
			}if($messageSource == 'user'){
				$eventId = $events['events'][0]['source']['userId'];
				$response = $bot->getProfile($uuid);
				if ($response->isSucceeded()) {
				$profile = $response->getJSONDecodedBody();
				$displayName = $profile['displayName'];
				}
			}

			$getProfile = $bot->getProfile($uuid);
			if ($getProfile->isSucceeded()) {
			$profile = $getProfile->getJSONDecodedBody();
			$displayName = $profile['displayName'];
			}
			
			$prefix = "!";
			$help_message = "Hello $displayName, nice to meet you ^_^ \nKeywords: help, sing, about, write, dota, games, manga, motw, ynm, chs, osu, steam, urban, lovecalc, anime, weather, stalk, music, usage, leave. \n\nFor help type :\n!usage <available keyword>";
			
			// $events1 = $bot->parseEventRequest($body, $signature);
			// $eventObj = $events1[0]; // Event Object ของ array แรก
			
			// log every event requests;
			$this->DB_M->logEvents($signature, $body, $messageSource, $eventType, $message, $displayName, $uuid);

			// Handle Postback
			$postback_data = $events['events'][0]['postback']['data'];
	
		}

		// additional functions
		function startsWith ($string, $startString) 
		{ 
    		$len = strlen($startString); 
    		return (substr($string, 0, $len) === $startString); 
		} 

		if ($messageSource == 'user') {
			$simpanNama = preg_replace('/[^\p{L}\s]/u','', $displayName);
			if(strtolower($message) == '!meme'){
				if(isset($profile['userId'])){
				$data = array(
					'userId' => $profile['userId'],
					'hostName' => $simpanNama
				);
				$this->DB_M->add_memer($data);
				$textMessageBuilder = new TextMessageBuilder("Upload your image to make a meme.\ntype '!cancel' if you want to cancel the command");
				$bot->replyMessage($replyToken, $textMessageBuilder);
				} else {
				$textMessageBuilder = new TextMessageBuilder("Upload your image to make a meme.\ntype '!cancel' if you want to cancel the command");
				$bot->replyMessage($replyToken, $textMessageBuilder);	
				}
			}

			if($events['events'][0]['message']['type'] == 'image'){
				$image = "https://rojokundo.xyz/tamako-ci/media/images/$messageId";
				 if ($this->DB_M->check_add_meme($profile['userId'], $image) == true){
					$this->textMessage("Image Upload Successfull!\ntype '!meme TopText-BottomText' to make a meme.\ntype '!cancel' if you want to cancel the command", $replyToken, $bot);	
				}
			}

			if(startsWith(strtolower($message), "!meme ")){
				$substr = substr($message, 6);
				$explode = explode("-",$substr);
				$toptext = strtoupper(rawurlencode($explode[0]));
				$bottext = strtoupper(rawurlencode($explode[1]));
				$get_meme = $this->DB_M->get_meme($profile['userId']);
				$gambar = $get_meme['linkGambar'];
				if(!empty($gambar) && !empty($toptext) && !empty($bottext)){
					$this->DB_M->cancel_meme($profile['userId']);
					$meme = "https://res.cloudinary.com/demo/image/fetch/c_scale,w_500/c_limit,co_white,g_south,l_text:impact_60_stroke:$bottext,w_400,y_20/c_limit,co_white,g_north,l_text:impact_60_stroke:$toptext,w_400,y_20/$gambar";
					$imageMessageBuilder = new ImageMessageBuilder("{$meme}","{$meme}");
					$bot->replyMessage($replyToken, $imageMessageBuilder);
				}else if(!empty($gambar) && !empty($toptext) && empty($bottext)){
					$this->DB_M->cancel_meme($profile['userId']);
					$meme = "https://res.cloudinary.com/demo/image/fetch/c_scale,w_500/c_limit,co_white,g_south,l_text:impact_60_stroke:$toptext,w_400,y_20/$gambar";
					$imageMessageBuilder = new ImageMessageBuilder("{$meme}","{$meme}");
					$bot->replyMessage($replyToken, $imageMessageBuilder);	
				}else if(empty($gambar) && !empty($toptext) || !empty($bottext)){
					$textMessage = new TextMessageBuilder("has not uploaded the image yet. type '!meme' to make meme!");
					$bot->replyMessage($replyToken, $textMessage);	
				}
			}

			if(strtolower($message) == '!cancel'){
				if ($this->DB_M->cancel_meme($profile['userId']) == true){
					$textMessage = new TextMessageBuilder("Successfully canceled command!");
					$bot->replyMessage($replyToken, $textMessage);
				} else {
					$this->textMessage("Nothing to cancel", $replyToken, $bot);
				}
			}
			
		}

		if ($profile['displayName'] == 'AFIF') {
			if ($message == 'testi'){
				$this->textMessage($eventType, $replyToken, $bot);
			}
		}
		

		if($events['events'][0]['message']['type'] == 'image'){
			if ($this->DB_M->check_recognition($eventId) == true) {
				$image = "https://rojokundo.xyz/tamako-ci/media/images/$messageId";
				$this->handle_recognition($image, $replyToken, $bot);
			}
		}

		if($events['events'][0]['type'] == 'postback'){
			if(startsWith($postback_data, "!sing ")){
				$this->handle_postback_sing($postback_data, $replyToken, $bot);
			}
			if(startsWith($postback_data, "/preview ")){
				$this->handle_postback_motw($postback_data, $replyToken, $bot);
			}
			if(startsWith($postback_data, "/play ")){
				$this->handle_postback_music($postback_data, $replyToken, $bot);
			}
		}	

		if(preg_match('(kontol|bangsat|memek|ngentot|fuck|sundala|lonte)', $message) === 1) {
			$array_message = ["Language pls -_-''","Astaghfirullah '-'","Watch your language ヽ(ﾟДﾟ)ﾉ","Dont be rude... be nice to others :)","-.-","If you dont mean what you say, SHUT THE F*CK UP"];
			shuffle($array_message);
			$this->textMessage($array_message[0], $replyToken, $bot);
		}

		if(startsWith($message, $prefix)){
			$substr_message = substr($message, 1);
			$explodeNot = explode(" ", $substr_message);
			if(strtolower($substr_message) == "help"){
				$this->textMessage($help_message, $replyToken, $bot);
			}else if(strtolower($substr_message) == 'sing'){
				$this->sing_tamako($replyToken, $bot);
			}else if(strtolower($substr_message) == 'about'){
				$this->aboutMessage($replyToken, $bot);
			}else if(startsWith($substr_message, "write ")){
				$teks = substr($substr_message, 7);
				$this->writeMessage($teks, $replyToken, $bot);
			}else if(startsWith($substr_message, "weather ")){
				$teks = substr($substr_message, 8);
				$this->weatherMessage($teks, $replyToken, $bot);
			}else if(startsWith($substr_message, "dota ")){
				$teks = substr($substr_message, 5);
				$this->dotaMessage($teks, $replyToken, $bot);
			}else if(startsWith($substr_message, "games ")){
				$teks = substr($substr_message, 6);
				$this->gamesMessage($teks, $replyToken, $bot);
			}else if(strtolower($substr_message) == 'ping'){
				$checkPing = $this->Data_M->ping("104.28.2.195", 443, 10);
				//$reply = "Ping to server $checkPing";
				$this->textMessage($checkPing, $replyToken, $bot);
			}else if(startsWith($substr_message, "manga ")){
				$teks = substr($substr_message, 6);
				$this->mangaMessage($teks, $replyToken, $bot, $messageSource);
			}else if(startsWith($substr_message, "anime ")){
				$teks = substr($substr_message, 6);
				$this->animeMessage($teks, $replyToken, $bot, $messageSource);
			}else if(strtolower($substr_message) == 'motw'){
				$this->motwMessage($replyToken, $bot);
			}else if(startsWith(strtolower($substr_message), "ynm ")){
				$substr_answer = substr($message, 5);
				$this->ynmMessage($substr_answer, $replyToken, $bot);
			}else if(startsWith(strtolower($substr_message), "chs ")){
				$teks = substr($substr_message, 4);
				$this->chsMessage($teks, $replyToken, $bot);
			}else if(startsWith(strtolower($substr_message), "osu ")){
				$teks = substr($substr_message, 4);
				$this->osuMessage($teks, $replyToken, $bot);
			}else if(startsWith(strtolower($substr_message), "steam ")){
				$teks = substr($substr_message, 6);
				$this->steamMessage($teks, $replyToken, $bot);
			}else if(startsWith(strtolower($substr_message), "urban ")){
				$teks = substr($substr_message, 6);
				$this->urbanMessage($teks, $replyToken, $bot);
			}else if(startsWith(strtolower($substr_message), "lovecalc ")){
				$teks = substr($substr_message, 9);
				$this->lovecalc($teks, $replyToken, $bot);
			}else if(startsWith(strtolower($substr_message), "stalk ")){
				$teks = substr($substr_message, 6);
				$this->stalkMessage($teks, $replyToken, $bot);
			}else if(startsWith(strtolower($substr_message), "tt ")){
				$teks = substr($substr_message, 3);
				$this->tiktokMessage($teks, $replyToken, $bot);
			}else if(startsWith(strtolower($substr_message), "music ")){
				$teks = substr($substr_message, 6);
				$this->musicMessage($teks, $replyToken, $bot);
			}else if(strtolower($substr_message) == 'recognition on'){
				$this->recognition($substr_message, $replyToken, $bot, $eventId, $displayName);
			}else if(strtolower($substr_message) == 'recognition off'){
				$this->recognition($substr_message, $replyToken, $bot, $eventId, $displayName);
			}else if(strtolower($substr_message) == 'tamako'){
				$this->tamakoMessage($profile, $replyToken, $bot);
			}else if(strtolower($substr_message) == 'info'){
				$this->imageMap_1($replyToken, $bot);
			}else if(startsWith(strtolower($substr_message), "usage ")){
				$teks = substr($substr_message, 6);
				$ini = $this->Data_M->usage_model($teks);
				$this->textMessage($ini, $replyToken, $bot);
			} else if (strtolower($substr_message) == 'leave') {
				$this->leaveMessage($messageSource, $eventId, $replyToken, $bot, $profile);
			} else if(startsWith(strtolower($substr_message), "st ")){
				$teks = substr($substr_message, 3);
				$this->stories_message($teks, $replyToken, $bot);
			}
			// else {
			// 	$this->textMessage("'$explodeNot[0]' not a keyword", $replyToken, $bot);
			// }
		}

		if(startsWith($message, "https://www.instagram.com/")){
			$this->instamediaMessage($message, $replyToken, $bot);
		}

		if(startsWith($message, "-")){
			$substr_answer = strtolower(substr($message, 1));
			if(isset($profile['userId'])){
				$repla = str_replace(" ", "%20", $substr_answer);
				$json = $this->Data_M->curl_mashape("http://api.brainshop.ai/get?bid=959&key=mRuGtfX59Y51uO9h&uid={$profile['userId']}&msg={$repla}");
				$this->textMessage("{$json['cnt']}",$replyToken, $bot);
			} else {
				$this->textMessage("Add me as a friend before using this feature :D", $replyToken, $bot);
			}
		}

		
    }

    public function textMessage($reply, $replyToken, $bot){
		$quickReply = new QuickReplyMessageBuilder([
			new QuickReplyButtonBuilder(new MessageTemplateActionBuilder("Help","!help")),
			new QuickReplyButtonBuilder(new MessageTemplateActionBuilder("Music of the week","!motw")),
			new QuickReplyButtonBuilder(new MessageTemplateActionBuilder("Instagram","!stalk 9gag")),
			new QuickReplyButtonBuilder(new MessageTemplateActionBuilder("Usage","!usage games")),
			new QuickReplyButtonBuilder(new MessageTemplateActionBuilder("Info","!info")),
			new QuickReplyButtonBuilder(new MessageTemplateActionBuilder("Guess The Anime BOT","Guess The Anime\nhttps://lin.ee/sPcKz5S")),
			new QuickReplyButtonBuilder(new MessageTemplateActionBuilder("Trakteer","https://trakteer.id/rojokundo")),
        ]);
        $textMessageBuilder = new TextMessageBuilder($reply,$quickReply);
		$response = $bot->replyMessage($replyToken, $textMessageBuilder);
		if ($response->isSucceeded()) {
			return;
		}
	}

	public function aboutMessage($replyToken, $bot){
		$buttonTemplateBuilder = new ButtonTemplateBuilder(
			'About Developer',
			'Rojokundo',
			'https://01d54fec-a-62cb3a1a-s-sites.googlegroups.com/site/untukaudio1/directory/tamakomem.jpg',
			[
			new UriTemplateActionBuilder('Youtube Channel','https://www.youtube.com/rojofactory'),
			new UriTemplateActionBuilder('Instagram','http://instagram.com/afifmakarim88'),
			new UriTemplateActionBuilder('Line@','https://line.me/R/ti/p/%40wfq6948b'),
			new UriTemplateActionBuilder('Donate','https://trakteer.id/rojokundo'),
			]
			);
			$templateMessage = new TemplateMessageBuilder('About Developer', $buttonTemplateBuilder);
			$bot->replyMessage($replyToken, $templateMessage);
	}

	private function handle_postback_sing($postback_data, $replyToken, $bot){
		$multiMessageBuilder = new MultiMessageBuilder();
			if(strtolower($postback_data) == "!sing dmr"){
			$multiMessageBuilder->add(new AudioMessageBuilder('https://sites.google.com/site/untukaudio1/directory/Dramatic%20Market%20Ride.m4a','60000'))
								->add(new TextMessageBuilder(
								"Tamako Market Opening Song \nPerformed by : \nKitashirakawa Tamako (CV: Suzaki Aya)"))
								->add(new ImageMessageBuilder('https://vignette2.wikia.nocookie.net/tamakomarket/images/e/e9/DramaticMarketRide.jpg/revision/latest?cb=20150314141448','https://vignette2.wikia.nocookie.net/tamakomarket/images/e/e9/DramaticMarketRide.jpg/revision/latest?cb=20150314141448'));
			return $bot->replyMessage($replyToken, $multiMessageBuilder);
			} else if(strtolower($postback_data) == "!sing neguse") {
				$multiMessageBuilder->add(new AudioMessageBuilder('https://sites.google.com/site/untukaudio1/directory/Neguse.m4a','60000'))
									->add(new TextMessageBuilder(
									"Tamako Market Ending Song \nPerformed by : \nKitashirakawa Tamako (CV: Suzaki Aya)"))
									->add(new ImageMessageBuilder('https://vignette4.wikia.nocookie.net/tamakomarket/images/7/79/Neguse_album.jpg/revision/latest?cb=20150314190015','https://vignette4.wikia.nocookie.net/tamakomarket/images/7/79/Neguse_album.jpg/revision/latest?cb=20150314190015'));
				$bot->replyMessage($replyToken, $multiMessageBuilder);
			} else if(strtolower($postback_data) == '!sing koinouta') {
				$multiMessageBuilder->add(new AudioMessageBuilder('https://sites.google.com/site/untukaudio1/directory/koinouta%20half.m4a','60000'))
									->add(new TextMessageBuilder(
									"Tamako Love Story Insert Song \nPerformed by : \nKitashirakawa Tamako (CV: Suzaki Aya)"))
									->add(new ImageMessageBuilder('https://i1.sndcdn.com/artworks-000096002566-vewto7-t500x500.jpg','https://i1.sndcdn.com/artworks-000096002566-vewto7-t500x500.jpg'));
				$bot->replyMessage($replyToken, $multiMessageBuilder);
			} else if(strtolower($postback_data) == '!sing principle') {
				$multiMessageBuilder->add(new AudioMessageBuilder('https://sites.google.com/site/untukaudio1/directory/principle%20half.m4a','60000'))
									->add(new TextMessageBuilder(
									"Tamako Love Story\nPerformed by : \nKitashirakawa Tamako (CV: Suzaki Aya)"))
									->add(new ImageMessageBuilder('https://68.media.tumblr.com/efa44a81455f9c46d61e600f98299e88/tumblr_n4stxffu2g1qc5wono1_500.png','https://68.media.tumblr.com/efa44a81455f9c46d61e600f98299e88/tumblr_n4stxffu2g1qc5wono1_500.png'));
				$bot->replyMessage($replyToken, $multiMessageBuilder);
			}
	}
	private function handle_postback_motw($postback_data, $replyToken, $bot)
	{
		$prev = substr($postback_data,9);	
		$json = $this->Data_M->curl_api("https://itunes.apple.com/lookup?id={$prev}");
		$getTrack = $json['results'][0]['previewUrl'];
		$getArtist = "{$json['results'][0]['artistName']} - {$json['results'][0]['trackName']}";
		$multiMessageBuilder = new MultiMessageBuilder();
		$multiMessageBuilder->add(new TextMessageBuilder("Now Playing :\n{$getArtist}"))
							->add(new AudioMessageBuilder("{$getTrack}",'30000'));	
		$bot->replyMessage($replyToken, $multiMessageBuilder);
	}

	private function handle_postback_music($postback_data, $replyToken, $bot){
		$prev = substr($postback_data, 6);
		$meleduk = explode("#", $prev);
		$multiMessageBuilder = new MultiMessageBuilder();
		$multiMessageBuilder->add(new TextMessageBuilder("Now Playing :\n{$meleduk[0]} - {$meleduk[1]}"))
							->add(new AudioMessageBuilder("{$meleduk[2]}",'30000'));	
		$bot->replyMessage($replyToken, $multiMessageBuilder);	
	}

	private function clean($string) {
		return preg_replace('/[\x00-\x1F\x7F-\xFF]/', '', $string); // Removes special chars.
	}

	public function sing_tamako($replyToken, $bot){
		$buttonTemplateBuilder = new ButtonTemplateBuilder(
			'Choose Tamako Song',
			'CV : Suzaki Aya',
			'https://s-media-cache-ak0.pinimg.com/564x/9e/fa/18/9efa18b56cd5057101bf72a0b023ad7f.jpg',
			[
			new PostbackTemplateActionBuilder('Dramatic Market Ride','!sing dmr'),
			new PostbackTemplateActionBuilder('Principle','!sing principle'),
			new PostbackTemplateActionBuilder('Koi No Uta','!sing koinouta'),
			new PostbackTemplateActionBuilder('Neguse','!sing neguse'),
			]
			);
			$templateMessage = new TemplateMessageBuilder('Song List', $buttonTemplateBuilder);
			return $bot->replyMessage($replyToken, $templateMessage);
	}

	private function writeMessage($message, $replyToken, $bot){

		$cleanText = $this->clean($message);
		$stats = rawurlencode($cleanText);
		if (strlen($cleanText)>=8 && strlen($cleanText)<=55) {
		$urlImage = "https://res.cloudinary.com/dftovjqdo/image/upload/a_-27,g_west,l_text:dark_name:{$stats},w_450,x_280,y_100/anime_notebook_yhekwa.jpg";
		$imageMessage = new ImageMessageBuilder("$urlImage","$urlImage");
		$bot->replyMessage($replyToken, $imageMessage);
		}else if (strlen($cleanText)<=8 && strlen($cleanText)<=55){
		$urlImages = "https://res.cloudinary.com/dftovjqdo/image/upload/a_-27,g_west,l_text:dark_name:{$stats},w_200,x_250,y_100/anime_notebook_yhekwa.jpg";
		$imageMessages = new ImageMessageBuilder("$urlImages","$urlImages");
		$bot->replyMessage($replyToken, $imageMessages);
		}else{
		$this->textMessage("text to long :(", $replyToken, $bot);
		}

	}

	private function weatherMessage($message, $replyToken, $bot) {

		$get_city = strtolower($message);
		$weather_api = $this->Data_M->curl_api("http://api.openweathermap.org/data/2.5/weather?q={$get_city},id&appid=9de670aed3147ecab224239b8ebd0d93&units=metric");
		$description = $weather_api['weather'][0]['description'];
		$temp = "". $weather_api['main']['temp'] . "°C";
		$city = $weather_api['name'];
		$country = $weather_api['sys']['country'];
		$weather_message = "it's $description and $temp in $city, $country";
		$this->textMessage($weather_message, $replyToken, $bot);

	}

	private function dotaMessage($message, $replyToken, $bot)
	{

		$vanity_url = strtolower($message); 
		$steam_api = $this->Data_M->curl_api("https://api.steampowered.com/ISteamUser/ResolveVanityURL/v0001/?key=7834436769DDB41F2D14A2F312377946&vanityurl={$vanity_url}");
		$get_id_64 = $steam_api['response']['steamid'];
		$get_id_32 = $this->Data_M->GET_32_BIT($get_id_64);

		$get_info = $this->Data_M->curl_api("https://api.opendota.com/api/players/{$get_id_32}");
		$get_winrate = $this->Data_M->curl_api("https://api.opendota.com/api/players/{$get_id_32}/wl");
		$dota_profile = $get_info['profile'];
		$signature_hero = $this->Data_M->curl_api("https://api.opendota.com/api/players/{$get_id_32}/heroes");
		$recent_match = $this->Data_M->curl_api("https://api.opendota.com/api/players/{$get_id_32}/recentMatches");
		$id_signature_hero = $this->Data_M->id_to_HeroName($signature_hero[0]['hero_id']);
		$id_match_hero = $this->Data_M->id_to_HeroName($recent_match[0]['hero_id']);
		$totalMatch = $get_winrate['win'] + $get_winrate['lose'];
		$kda =  $recent_match[0]['kills'] ."/"  . $recent_match[0]['deaths'] . "/" .$recent_match[0]['assists'];
		$lh = $recent_match[0]['last_hits'] . "/" . $recent_match[0]['gold_per_min'];

		if(isset($dota_profile['personaname'])){
			$arrayDota = [
				'displayImage' => $dota_profile['avatarfull'],
				'persona' => $dota_profile['personaname'],
				'win' => $get_winrate['win'],
				'totalMatch' => $totalMatch,
				'signatureHero' => $id_signature_hero,
				'lastMatchHero' => $id_match_hero,
				'kda' => $kda,
				'lh' => $lh,
				'url_dotabuff' => "https://www.dotabuff.com/matches/{$recent_match[0]['match_id']}",
				'url_steam' => $dota_profile['profileurl']
			];
			$flex = $this->Data_M->flexMessageDota($arrayDota);
			$flexMessageBuilder = new RawMessageBuilder([
				'type' => 'flex',
				'altText' => 'Dota Info',
				'contents' => json_decode($flex)
			]);
			return $bot->replyMessage($replyToken, $flexMessageBuilder);

		} else {
			$this->textMessage("Dota 2 information not found", $replyToken, $bot);
		}
	}

	private function gamesMessage($message, $replyToken, $bot) {
		
		$key = str_replace(" ", "%20", strtolower($message)); 
		$giantbomb_api = $this->Data_M->curl_api_header("https://www.giantbomb.com/api/search/?api_key=a0bede1760f86f2f59ff3ac477c953fed643ea0b&resources=game&query={$key}&format=json&limit=5");
		$results = $giantbomb_api['results'];
		if (isset($results[0]['name'])) {
		for($i = 0; $i < count($results); $i++) {
			$items_platform = array();
			$game_name = !empty($results[$i]['name']) ? $results[$i]['name'] :  "-";
			$release_date = !empty($results[$i]['original_release_date']) ? $results[$i]['original_release_date'] :  "-";
			$images = !empty($results[$i]['image']['medium_url']) ? $results[$i]['image']['medium_url'] : "https://i.pinimg.com/originals/b5/2d/66/b52d66473ac0f3c887040948af057236.jpg";
			$desc = !empty($results[$i]['deck']) ? str_replace('"', "", $results[$i]['deck']) :  "-";
			$url = !empty($results[$i]['site_detail_url']) ? $results[$i]['site_detail_url'] :  "-";

			foreach ($results[$i]['platforms'] as $platforms) {
				$items_platform[] = $platforms['name'];
				$plat = implode(', ', $items_platform);
			}

			$content[] = '{
				"type": "bubble",
				"hero": {
				  "type": "image",
				  "url": "'.$images.'",
				  "size": "full",
				  "aspectRatio": "8:9",
				  "aspectMode": "cover"
				},
				"body": {
				  "type": "box",
				  "layout": "vertical",
				  "contents": [
					{
					  "type": "text",
					  "text": "'.$game_name.'",
					  "weight": "bold",
					  "size": "xl",
					  "wrap": true
					},
					{
					  "type": "box",
					  "layout": "vertical",
					  "margin": "lg",
					  "spacing": "sm",
					  "contents": [
						{
						  "type": "box",
						  "layout": "baseline",
						  "spacing": "sm",
						  "contents": [
							{
							  "type": "text",
							  "text": "Release Date",
							  "color": "#aaaaaa",
							  "size": "sm",
							  "flex": 3,
							  "wrap": true
							},
							{
							  "type": "text",
							  "text": "'.$release_date.'",
							  "wrap": true,
							  "color": "#666666",
							  "size": "sm",
							  "flex": 5
							}
						  ]
						},
						{
						  "type": "box",
						  "layout": "baseline",
						  "spacing": "sm",
						  "contents": [
							{
							  "type": "text",
							  "text": "Platform",
							  "color": "#aaaaaa",
							  "size": "sm",
							  "flex": 3
							},
							{
							  "type": "text",
							  "text": "'.$plat.'",
							  "wrap": true,
							  "color": "#666666",
							  "size": "sm",
							  "flex": 5
							}
						  ]
						}
					  ]
					},
					{
					  "type": "box",
					  "layout": "vertical",
					  "contents": [
						{
						  "type": "text",
						  "text": "Description",
						  "weight": "bold",
						  "size": "sm"
						},
						{
						  "type": "box",
						  "layout": "vertical",
						  "contents": [
							{
							  "type": "text",
							  "text": "'.$desc.'",
							  "margin": "lg",
							  "size": "sm",
							  "wrap": true
							}
						  ],
						  "paddingTop": "5px"
						}
					  ],
					  "margin": "xl",
					  "cornerRadius": "2px"
					}
				  ],
				  "backgroundColor": "#ffffff"
				},
				"footer": {
				  "type": "box",
				  "layout": "vertical",
				  "spacing": "sm",
				  "contents": [
					{
					  "type": "button",
					  "style": "link",
					  "height": "sm",
					  "action": {
						"type": "uri",
						"label": "Open Browser",
						"uri": "'.$url.'"
					  }
					},
					{
						"type": "spacer",
						"size": "sm"
					  }
					],
					"flex": 0
				  }
				}';
		
			}

		  $json = '{
			  "type": "carousel",
			  "contents": [' . implode(",",$content) . ']
			}';

		$flexMessageBuilder = new RawMessageBuilder([
			'type' => 'flex',
			'altText' => 'Video Game Info',
			'contents' => json_decode($json)
		]);
		return $bot->replyMessage($replyToken, $flexMessageBuilder);
		} else {
			$this->textMessage("Game information not found", $replyToken, $bot);
		}
		
	}

	private function mangaMessage($message, $replyToken, $bot, $messageSource)
	{

		$spaceRep = str_replace(" ", "%20", strtolower($message));
		$manga_api = $this->Data_M->curl_api("https://kitsu.io/api/edge/manga?filter[text]={$spaceRep}&page[limit]=1&page[offset]=0");
		$data = $manga_api['data'][0]['attributes'];
		$get_genre= $manga_api['data'][0]['relationships']['genres']['links']['related'];
		$genre_api = $this->Data_M->curl_api($get_genre);

		$output = array();

		foreach($genre_api['data'] as $result) {
		$output[] = $result['attributes']['name'];
		$genre = implode(', ', $output);
		}

		$short_synopsis = strlen($data['synopsis']) > 200 ? substr($data['synopsis'],0,200)."... (DM to Read More)" : $data['synopsis'];
		$multiMessageBuilder = new MultiMessageBuilder();

		if(isset($data['canonicalTitle']) && $messageSource == 'room' || $messageSource =='group'){

			
			$multiMessageBuilder->add(new TextMessageBuilder("{$data['canonicalTitle']}\nStatus : {$data['status']}\nRating : {$data['averageRating']}\nGenre : {$genre}\nSynopsis : {$short_synopsis}"))
								->add(new ImageMessageBuilder("{$data['posterImage']['original']}","{$data['posterImage']['original']}"));	
			$bot->replyMessage($replyToken, $multiMessageBuilder);	

		} else if(isset($data['canonicalTitle']) && $messageSource == 'user'){
			$multiMessageBuilder->add(new TextMessageBuilder("{$data['canonicalTitle']}\nStatus : {$data['status']}\nRating : {$data['averageRating']}\nGenre : {$genre}\nSynopsis : {$data['synopsis']}"))
								->add(new ImageMessageBuilder("{$data['posterImage']['original']}","{$data['posterImage']['original']}"));	
			$bot->replyMessage($replyToken, $multiMessageBuilder);	
		} else {
			$this->textMessage("Can't find manga", $replyToken, $bot);
		}

	}

	private function animeMessage($message, $replyToken, $bot, $messageSource)
	{
		$spaceRep = str_replace(" ", "%20", strtolower($message));
		$anime_api = $this->Data_M->curl_api("https://kitsu.io/api/edge/anime?filter[text]={$spaceRep}&page[limit]=1&page[offset]=0");
		$data = $anime_api['data'][0]['attributes'];
		$get_genre= $anime_api['data'][0]['relationships']['genres']['links']['related'];
		$genre_api = $this->Data_M->curl_api($get_genre);

		$output = array();

		foreach($genre_api['data'] as $result) {
		$output[] = $result['attributes']['name'];
		$genre = implode(', ', $output);
		}

		$short_synopsis = strlen($data['synopsis']) > 200 ? substr($data['synopsis'],0,200)."... (DM to Read More)" : $data['synopsis'];
		
		$multiMessageBuilder = new MultiMessageBuilder();

		if(isset($data['canonicalTitle']) && $messageSource == 'room' || $messageSource =='group'){

			$multiMessageBuilder->add(new TextMessageBuilder("".$data['canonicalTitle']."\nType : ".$data['showType']."\nStatus : ".$data['status']."\nTotal Episode : ".$data['episodeCount']."\nGenre : ".$genre."Synopsis :\n".$short_synopsis.""))
								->add(new ImageMessageBuilder("".$data['posterImage']['medium']."","".$data['posterImage']['medium'].""));	
			$bot->replyMessage($replyToken, $multiMessageBuilder);	

		} else if(isset($data['canonicalTitle']) && $messageSource == 'user'){
			$multiMessageBuilder->add(new TextMessageBuilder("".$data['canonicalTitle']."\nType : ".$data['showType']."\nStatus : ".$data['status']."\nTotal Episode : ".$data['episodeCount']."\nGenre : ".$genre."\nSynopsis :\n".$data['synopsis'].""))
								->add(new ImageMessageBuilder("".$data['posterImage']['medium']."","".$data['posterImage']['medium'].""));
			$bot->replyMessage($replyToken, $multiMessageBuilder);	
		} else {
			$this->textMessage("Can't find anime", $replyToken, $bot);
		}
	}

	private function motwMessage($replyToken, $bot)
	{
		$json = $this->Data_M->curl_api("https://rss.itunes.apple.com/api/v1/id/apple-music/top-songs/all/10/explicit.json");
		$imgUrl = "https://cdn.dribbble.com/users/90627/screenshots/3171301/applemusiclogodribble.png";

		for( $i=0; $i < 10; $i++ ){
			$prevSong = $json['feed']['results'][$i]['id'];
			$trackget = $json['feed']['results'][$i]['name'];
			$trackget40 = strlen($trackget) > 37 ? substr($trackget,0,37)."..." : $trackget;
			$artistget = $json['feed']['results'][$i]['artistName'];
			$artistget40 = strlen($artistget) > 37 ? substr($artistget,0,37)."..." : $artistget;
			$actions = array(new PostbackTemplateActionBuilder("Preview Song","/preview {$prevSong}"),
		);

		$column = new CarouselColumnTemplateBuilder("{$artistget40}", "{$trackget40}", $imgUrl , $actions);
		$columns[] = $column;

		}

		$carousel = new CarouselTemplateBuilder($columns);
		$multiMessageBuilder = new MultiMessageBuilder();
		$multiMessageBuilder->add(new TextMessageBuilder('10 Music of the week based on Itunes Indonesia'))
							->add(new TemplateMessageBuilder("Music Chart", $carousel));	
		$bot->replyMessage($replyToken, $multiMessageBuilder);
	}

	private function ynmMessage($userMessageSubs, $replyToken, $bot)
	{	
		if (strlen($userMessageSubs) == 0 ) {
			$this->textMessage("Empty statement", $replyToken, $bot);
		} 
		$textMessageBuilder = array(new TextMessageBuilder("{$userMessageSubs}\n\nYes"),
		new TextMessageBuilder("{$userMessageSubs}\n\nNo"),
		new TextMessageBuilder("{$userMessageSubs}\n\nMaybe"));
		$random_str = $textMessageBuilder[array_rand($textMessageBuilder)];
		$bot->replyMessage($replyToken, $random_str);
	}

	private function chsMessage($message, $replyToken, $bot)
	{
		$userMessageCHS = explode('-', $message);
		$random_str = $userMessageCHS[array_rand($userMessageCHS)];
		$repl = str_replace("!chs ", "", $random_str);
		$textMessageBuilder = new TextMessageBuilder("I choose " .$repl. "!");
		$bot->replyMessage($replyToken, $textMessageBuilder);
	}


	private function osuMessage($message, $replyToken, $bot)
	{
		$mania_api = $this->Data_M->curl_api("https://osu.ppy.sh/api/get_user?u={$message}&m=3&k=1958afa9967f399f1cd22f52be34d93bcf755212");
		$taiko_api = $this->Data_M->curl_api("https://osu.ppy.sh/api/get_user?u={$message}&m=1&k=1958afa9967f399f1cd22f52be34d93bcf755212");
		$ctbApi = $this->Data_M->curl_api("https://osu.ppy.sh/api/get_user?u={$message}&m=2&k=1958afa9967f399f1cd22f52be34d93bcf755212");
		$std_api = $this->Data_M->curl_api("https://osu.ppy.sh/api/get_user?u={$message}&m=0&k=1958afa9967f399f1cd22f52be34d93bcf755212");

		if ($mania_api[0]['username']) {
		$array = [
			'mania' => [
				'accuracy' => $mania_api[0]['accuracy'],
				'countryRank' => $mania_api[0]['pp_country_rank'],
				'globalRank' => $mania_api[0]['pp_rank'],
				'username' => $mania_api[0]['username'],
				'join_date' => $mania_api[0]['join_date'],
				'country' => $this->Data_M->code_to_country($mania_api[0]['country']),
				'pp' => "https://a.ppy.sh/{$mania_api[0]['user_id']}"
			],

			'taiko_api' => [
				'accuracy' => $taiko_api[0]['accuracy'],
				'countryRank' => $taiko_api[0]['pp_country_rank'],
				'globalRank' => $taiko_api[0]['pp_rank'],
			],

			'ctb_api' => [
				'accuracy' => $ctbApi[0]['accuracy'],
				'countryRank' => $ctbApi[0]['pp_country_rank'],
				'globalRank' => $ctbApi[0]['pp_rank']
			],

			'std_api' => [
				'accuracy' => $std_api[0]['accuracy'],
				'countryRank' => $std_api[0]['pp_country_rank'],
				'globalRank' => $std_api[0]['pp_rank']
			]
		];
		$json = $this->Data_M->flexMessageOsu($array);
		$flexMessageBuilder = new RawMessageBuilder([
			'type' => 'flex',
			'altText' => 'Info Osu',
			'contents' => json_decode($json)
		]);
		return $bot->replyMessage($replyToken, $flexMessageBuilder);
		} else {
			$this->textMessage("Can't find osu username", $replyToken, $bot);
		}
	}

	private function steamMessage($message, $replyToken, $bot)
	{
		$steam_id = $this->Data_M->curl_api("http://api.steampowered.com/ISteamUser/ResolveVanityURL/v0001/?key=7834436769DDB41F2D14A2F312377946&vanityurl={$message}");
			$profile_id = $steam_id['response']['steamid'];
			$steam_user = $this->Data_M->curl_api("http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=7834436769DDB41F2D14A2F312377946&steamids={$profile_id}");
			$get_played = $this->Data_M->curl_api("https://api.steampowered.com/IPlayerService/GetRecentlyPlayedGames/v0001/?key=7834436769DDB41F2D14A2F312377946&steamid={$profile_id}&count=5&format=json");
			if (isset($profile_id)) {		
					if (isset($get_played['response']['games'])){
					foreach($get_played['response']['games'] as $result_game) {
						$recent_game_name[] = $result_game['name'];
						$recent_game_hours[] = floor($result_game['playtime_forever'] / 60);
					}
					} else {
						$recent_game_name = '-';
						$recent_game_hours = '-';
					}
		
				$arrayProfile = [
					'personaname' => !empty($steam_user['response']['players'][0]['personaname']) ? $steam_user['response']['players'][0]['personaname'] :  "-",
					'profilestate' => $this->Data_M->steam_state($steam_user['response']['players'][0]['personastate']),
					'avatarfull' => !empty($steam_user['response']['players'][0]['avatarfull']) ? $steam_user['response']['players'][0]['avatarfull'] :  "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_960_720.png",
					'realname' => !empty($steam_user['response']['players'][0]['realname']) ? $steam_user['response']['players'][0]['realname'] :  "-",
					'profile_url' => !empty($steam_user['response']['players'][0]['profileurl']) ? $steam_user['response']['players'][0]['profileurl'] :  "-",
				];
				
		$json = $this->Data_M->flexMessageSteam($arrayProfile, $recent_game_name, $recent_game_hours);
		$flexMessageBuilder = new RawMessageBuilder([
			'type' => 'flex',
			'altText' => 'Steam Info',
			'contents' => json_decode($json)
		]);
		return $bot->replyMessage($replyToken, $flexMessageBuilder);
		} else {
			$this->textMessage("Can't find steam username", $replyToken, $bot);
		}
	}

	private function urbanMessage($message, $replyToken, $bot){
		$spaceRep = str_replace(" ", "%20", $message);
		$slang_api = $this->Data_M->curl_mashape("https://mashape-community-urban-dictionary.p.mashape.com/define?term={$spaceRep}");
		$defin = $slang_api['list'][0]['definition'];
		$word = $slang_api['list'][0]['word'];
		$example = $slang_api['list'][0]['example'];
		if(array_key_exists('definition',$slang_api['list'][0])){	
			$this->textMessage("Word: {$word}\nDefinition:\n{$defin}\n\nEx: {$example}", $replyToken, $bot);
		}
		else{
			$this->textMessage("Urban word ''{$message}'' cannot be found", $replyToken, $bot);
		}
	}

	private function lovecalc($message, $replyToken, $bot){
		$explodeLove = explode('-', $message);
		$spaceRep = str_replace(" ", "%20", $explodeLove);
		$lovecalc_api = $this->Data_M->curl_mashape("https://love-calculator.p.mashape.com/getPercentage?fname={$spaceRep[0]}&sname={$spaceRep[1]}");
			
			$yName = $lovecalc_api['fname'];
			$cName = $lovecalc_api['sname'];
			$kapital = ucfirst($yName);
			$kapital1 = ucfirst($cName);

		if( !empty( $lovecalc_api['fname'] ) && $lovecalc_api['sname'] ){
		$this->textMessage("".$kapital." and ".$kapital1." has ".$lovecalc_api['percentage']."% love \nResults : ".$lovecalc_api['result']."", $replyToken, $bot);
		} else {
			$this->textMessage("Incorrect format XD");
		}
	}

	private function stalkMessage($message, $replyToken, $bot){
		$ig_medias = $this->Instagram_M->get_instagram_media($message);
		$angka = count($ig_medias);
		shuffle($ig_medias);
		if($ig_medias[0]){
			for($i=0;$i<$angka; $i++){
				if(($i+1)<=10){
					$imageUrl = $ig_medias[$i]['imageHighResolutionUrl'];
					$fetch = $ig_medias[$i]['link'];
					$actions = new MessageTemplateActionBuilder("Get Media","{$fetch}");
	
					$column = new ImageCarouselColumnTemplateBuilder($imageUrl , $actions);
					$columns[] = $column;
				}
		}
			$carousel = new ImageCarouselTemplateBuilder($columns);
			$templateMessage = new TemplateMessageBuilder('Stalk Instagram',$carousel);
			$bot->replyMessage($replyToken, $templateMessage);
		} else {
			$this->textMessage("username not found or account is private", $replyToken, $bot);
		}
	}

	private function tiktokMessage($message, $replyToken, $bot){
		$user_tiktok = json_decode(file_get_contents("https://tiktok-brainans-scraper.herokuapp.com/user/{$message}"), true);
		$angka = count($user_tiktok['data']);
		// shuffle($user_tiktok);
		if($user_tiktok['data'][0]){
			for($i=0;$i<$angka; $i++){
				if(($i+1)<=10){
					$imageUrl = $user_tiktok['data'][$i]['thumbnail_src'];
					$fetch = base64_encode($user_tiktok['data'][$i]['url']);
					$views = $user_tiktok['data'][$i]['views'];
					
					$actions = new UriTemplateActionBuilder("Open Browser","https://rojokundo.xyz/view_tiktok.php?video_url={$fetch}");
	
					$column = new ImageCarouselColumnTemplateBuilder($imageUrl , $actions);
					$columns[] = $column;
				}
		}
			$carousel = new ImageCarouselTemplateBuilder($columns);
			$templateMessage = new TemplateMessageBuilder('Tik Tok',$carousel);
			$bot->replyMessage($replyToken, $templateMessage);
		} else {
			$this->textMessage("username not found or account is private", $replyToken, $bot);
		}
		// $user_tiktok = file_get_contents("https://tiktok-brainans-scraper.herokuapp.com/user/{$message}");
		// $get_song = $user_tiktok['data'];
		// $hitung = count($get_song);
		// shuffle($get_song);
		// $imgUrl = "https://1.bp.blogspot.com/-5xyNqP5DiOs/UhsqtOh78ZI/AAAAAAAAAlo/0KMQxRxZZqI/s1600/Logo+iTunes.JPG";
		// for($i=0;$i<$hitung; $i++){
		// 	if(($i+1)<=10){
		// 		$artistget = $get_song[$i]['artistName'];
		// 		$artistget40 = strlen($artistget) > 37 ? substr($artistget,0,37)."..." : $artistget;
		// 		$trackget = $get_song[$i]['trackCensoredName'];
		// 		$trackget40 = strlen($trackget) > 37 ? substr($trackget,0,37)."..." : $trackget;
		// 		$buythis = $get_song[$i]['trackViewUrl'];
		// 		$saveSong1 = $get_song[$i]['previewUrl'];
		// 		$actions = array(new UriTemplateActionBuilder("Buy Track","{$buythis}"),
		// 			new PostBackTemplateActionBuilder("Preview Track","/play {$artistget}#{$trackget}#{$saveSong1}"),
		// 		);
		// 		$column = new CarouselColumnTemplateBuilder("{$artistget40}", "{$trackget40}", $imgUrl , $actions);
		// 		$columns[] = $column;
		// 	}
		// }
		// if ($hitung !== 0) {
		// 	$carousel = new CarouselTemplateBuilder($columns);
		// 	$multiMessageBuilder = new MultiMessageBuilder();
		// 	$multiMessageBuilder->add(new TextMessageBuilder("Popular Track by {$get_song[0]['artistName']}\n{$get_song[0]['artistViewUrl']}"))
		// 						->add(new TemplateMessageBuilder("Itunes Music", $carousel));	
		// 	$bot->replyMessage($replyToken, $multiMessageBuilder);
		// } else {
		// 	$this->textMessage("'$message' not found", $replyToken, $bot);
		// }
	}

	private function musicMessage($message, $replyToken, $bot) 
	{
		$spacePlus = str_replace(" ", "+", $message);
		$itunes = $this->Data_M->curl_api("https://itunes.apple.com/search?term={$spacePlus}&media=music&limit=25&country=id");
		$get_song = $itunes['results'];
		$hitung = count($get_song);
		shuffle($get_song);
		$imgUrl = "https://1.bp.blogspot.com/-5xyNqP5DiOs/UhsqtOh78ZI/AAAAAAAAAlo/0KMQxRxZZqI/s1600/Logo+iTunes.JPG";
		for($i=0;$i<$hitung; $i++){
			if(($i+1)<=10){
				$artistget = $get_song[$i]['artistName'];
				$artistget40 = strlen($artistget) > 37 ? substr($artistget,0,37)."..." : $artistget;
				$trackget = $get_song[$i]['trackCensoredName'];
				$trackget40 = strlen($trackget) > 37 ? substr($trackget,0,37)."..." : $trackget;
				$buythis = $get_song[$i]['trackViewUrl'];
				$saveSong1 = $get_song[$i]['previewUrl'];
				$actions = array(new UriTemplateActionBuilder("Buy Track","{$buythis}"),
					new PostBackTemplateActionBuilder("Preview Track","/play {$artistget}#{$trackget}#{$saveSong1}"),
				);
				$column = new CarouselColumnTemplateBuilder("{$artistget40}", "{$trackget40}", $imgUrl , $actions);
				$columns[] = $column;
			}
		}
		if ($hitung !== 0) {
			$carousel = new CarouselTemplateBuilder($columns);
			$multiMessageBuilder = new MultiMessageBuilder();
			$multiMessageBuilder->add(new TextMessageBuilder("Popular Track by {$get_song[0]['artistName']}\n{$get_song[0]['artistViewUrl']}"))
								->add(new TemplateMessageBuilder("Itunes Music", $carousel));	
			$bot->replyMessage($replyToken, $multiMessageBuilder);
		} else {
			$this->textMessage("'$message' not found", $replyToken, $bot);
		}

	}

	private function instamediaMessage($message, $replyToken, $bot)
	{
		Unirest\Request::verifyPeer(false);
		 $instagram = \InstagramScraper\Instagram::withCredentials("sinergi08", "indonesia92", new Psr16Adapter('Files'));
        $instagram->login(true);
        $instagram->saveSession();
		$media = $instagram->getMediaByUrl($message);
		$gambar = $media->getImageHighResolutionUrl();
		if($media->getType() == 'video'){
		$video_url = $media['videoStandardResolutionUrl'];		
		$VideoMessage = new VideoMessageBuilder("{$video_url}","{$gambar}");
		$bot->replyMessage($replyToken, $VideoMessage);	
		} else if($media->getType() == 'image'){
		$ImageMessage = new ImageMessageBuilder("{$gambar}","{$gambar}");
		$bot->replyMessage($replyToken, $ImageMessage);
		} else if($media->getType() == 'sidecar'){
		$carousel = $media->getSidecarMedias();
		$countCar = count($carousel);
		$multiMessageBuilder = new MultiMessageBuilder();
		for($i=0;$i<$countCar; $i++){
			if(($i+1)<=5){
				$foto = $carousel[$i]["imageHighResolutionUrl"];
				$isi = new ImageMessageBuilder("$foto","$foto", $quickReply);
				$multiMessageBuilder->add($isi);
			}
		}		
		$bot->replyMessage($replyToken, $multiMessageBuilder);
		} else {
		$this->textMessage("post not found or in private", $replyToken, $bot);
		}
	}

	private function leaveMessage($messageSource, $eventId, $replyToken, $bot, $profile){
			if(isset($profile['userId'])){
			if($messageSource == 'group'){
				$textMessageBuilder = new TextMessageBuilder("bye bye~");
				$result = $bot->replyMessage($replyToken, $textMessageBuilder);
				$bot->leaveGroup($eventId, $result);
			} else if($messageSource == 'room'){
				$textMessageBuilder = new TextMessageBuilder("bye bye~");
				$result = $bot->replyMessage($replyToken, $textMessageBuilder);
				$bot->leaveRoom($eventId, $result);
			} else {
				$textMessage = new TextMessageBuilder("1:1 Chat");
				$bot->replyMessage($replyToken, $textMessage);
			}
			} else {
				$textMessage = new TextMessageBuilder("Add me as a friend before using this feature :D");
				$bot->replyMessage($replyToken, $textMessage);
			}
	}

	private function recognition($message, $replyToken, $bot, $eventId, $displayName)
	{	
		$substr = substr($message, 12);
		$data = [
			'groupId' => $eventId,
			'username' => $displayName
		];
		if ($substr == 'on') {
			if($this->DB_M->add_recognition($data) == true){
				$this->textMessage("Face Recognition ON!", $replyToken, $bot);
			} else {
				$this->textMessage("Face Recognition already ON\nType '!recognition off' to turn off face recognition features", $replyToken, $bot);
			}
		}

		if ($substr == 'off') {
			if($this->DB_M->delete_recognition($eventId) == true){
				$this->textMessage("Face Recognition OFF!", $replyToken, $bot);
			} else {
				$this->textMessage("Face Recognition already OFF\nType '!recognition on' to turn on face recognition features", $replyToken, $bot);
			}
		}
	}

	private function handle_recognition($image, $replyToken, $bot)
	{
		$response = $this->Data_M->curl_api("https://api-us.faceplusplus.com/facepp/v3/detect?api_key=7-b-6VyLh8ydCg5CXKrYzuOErsDq3GBZ&api_secret=KwQQy8lsgj6AHO3gTwKs9y7RC-1ClXGf&image_url={$image}&return_attributes=gender%2Cage%2Cemotion%2Cethnicity%2Cbeauty", "POST");
		if ($response['faces'][0]['attributes']['gender']['value'] == "Female"){
			$blevel = $response['faces'][0]['attributes']['beauty']['female_score'];
			$param = "Beauty Level";
		}else{
			$blevel = $response['faces'][0]['attributes']['beauty']['male_score'];
			$param = "Handsome Level";
		}
			$percent = round($blevel) . '%';
			$value = max($response['faces'][0]['attributes']['emotion']);
			$emo = array_search($value, $response['faces'][0]['attributes']['emotion']);
		if(isset($response['faces'][0]['attributes']['gender']['value'])) {
			$textMessage = "Face Recognition Results:\nGender : {$response['faces'][0]['attributes']['gender']['value']}\nApproximate Age : {$response['faces'][0]['attributes']['age']['value']}\nEthnic : {$response['faces'][0]['attributes']['ethnicity']['value']}\nEmotions :\n-{$emo} : {$value}\n$param : {$percent}\n\nType '!recognition off' to disable face recognition features.";
			$this->textMessage($textMessage, $replyToken, $bot);
		}
	}

	private function tamakoMessage($profile, $replyToken, $bot){
		if(isset($profile['userId'])){
			$namalu = $this->clean($profile['displayName']);
			$status = $profile['statusMessage'];
			$cleaned = $this->clean($status);
			$x = (isset($status)) ? "$cleaned" : '...';

			$name = str_replace(" ","%20", $namalu);
			$stats = str_replace(" ","%20", $x);
			$thumb = "https://res.cloudinary.com/dftovjqdo/image/upload/a_0,g_south_west,l_text:dark_name:{$name},x_95,y_85/loveletter_by_animekennelrpg-d9qu63r_wzv4rn";
			$randoms = array("https://res.cloudinary.com/dftovjqdo/image/upload/a_-6,g_north_west,l_text:dark_name:{$name},x_130,y_60/tamako_axpdrq.jpg",
				"https://res.cloudinary.com/dftovjqdo/image/upload/a_0,g_center,l_text:white_name:-{$name}-,x_0,y_-200/g_center,l_text:white_name:{$stats},y_-100/xexexe_xczhy3.jpg");
			$random_str = $randoms[array_rand($randoms)];
			
			$imageMessage = new ImageMessageBuilder($random_str, $thumb);
			$bot->replyMessage($replyToken, $imageMessage);
		} else {
			$this->textMessage("Not a friend", $replyToken, $bot);
		}
	}

	private function imageMap_1($replyToken, $bot){
		$richMessageUrl = "https://rojokundo.xyz/images/tamako1.png?_ignored=";
		$random = ["!motw","!steam endthought","!sing","!osu wiam103","https://trakteer.id/rojokundo"];
		shuffle($random);
		$imagemapMessageBuilder = new ImagemapMessageBuilder(
			$richMessageUrl,
			'Info',
			new BaseSizeBuilder(1040, 1040),
			[
				new ImagemapMessageActionBuilder(
					'!help',
					new AreaBuilder(0, 0, 520, 520)
				),
				new ImagemapMessageActionBuilder(
					'!motw',
					new AreaBuilder(520, 0, 520, 520)
				),
				new ImagemapMessageActionBuilder(
					'!anime tamako market',
					new AreaBuilder(0, 520, 520, 520)
				),
				new ImagemapMessageActionBuilder(
					$random[0],
					new AreaBuilder(520, 520, 520, 520)
				)
			]
		);
		$bot->replyMessage($replyToken, $imagemapMessageBuilder);

	}

	private function stories_message($message, $replyToken, $bot) {
		
		$key = str_replace(" ", "%20", strtolower($message)); 
		$stories_list = $this->Instagram_M->insta_stories($key);
		rsort($stories_list);
		if($stories_list == null){
			$this->textMessage("username not found or account is private", $replyToken, $bot);
		} else {
			for($i = 0; $i < count($stories_list); $i++) {
				if(($i+1)<=10){
				// $url = !empty($stories_list[$i]['videoLowBandwidthUrl']) ? $stories_list[$i]['videoLowBandwidthUrl'] : $stories_list[$i]['imageStandardResolutionUrl'];
				$url = empty($stories_list[$i]['videoLowBandwidthUrl']) ? $stories_list[$i]['imageStandardResolutionUrl'] : $stories_list[$i]['videoLowBandwidthUrl'];
				$content[] = '{
					"type": "bubble",
					"body": {
					  "type": "box",
					  "layout": "vertical",
					  "contents": [
						{
						  "type": "image",
						  "url": "'.$stories_list[$i]['imageStandardResolutionUrl'].'",
						  "size": "full",
						  "aspectMode": "cover",
						  "aspectRatio": "1:2",
						  "gravity": "top"
						},
						{
						  "type": "box",
						  "layout": "vertical",
						  "contents": [
							{
							  "type": "box",
							  "layout": "vertical",
							  "contents": [
								{
								  "type": "filler"
								},
								{
								  "type": "box",
								  "layout": "baseline",
								  "contents": [
									{
									  "type": "filler"
									},
									{
									  "type": "text",
									  "text": "Open Browser",
									  "color": "#ffffff",
									  "flex": 0,
									  "offsetTop": "-2px"
									},
									{
									  "type": "filler"
									}
								  ],
								  "spacing": "sm"
								},
								{
								  "type": "filler"
								}
							  ],
							  "borderWidth": "1px",
							  "cornerRadius": "4px",
							  "spacing": "sm",
							  "borderColor": "#ffffff",
							  "margin": "xxl",
							  "height": "40px",
							  "action": {
								"type": "uri",
								"label": "action",
								"uri": "'.$url.'"
							  }
							}
						  ],
						  "position": "absolute",
						  "offsetBottom": "0px",
						  "offsetStart": "0px",
						  "offsetEnd": "0px",
						  "backgroundColor": "#03303Acc",
						  "paddingAll": "20px",
						  "paddingTop": "18px"
						},
						{
						  "type": "box",
						  "layout": "vertical",
						  "contents": [
							{
							  "type": "text",
							  "text": "'.$this->Data_M->time2str($stories_list[$i]['createdTime']).'",
							  "color": "#ffffff",
							  "align": "center",
							  "size": "xs",
							  "offsetTop": "3px"
							}
						  ],
						  "position": "absolute",
						  "cornerRadius": "20px",
						  "offsetTop": "18px",
						  "backgroundColor": "#ff334b",
						  "offsetStart": "18px",
						  "height": "25px",
						  "width": "100px"
						}
					  ],
					  "paddingAll": "0px"
					}
				  }';
			
				}
			}
	
			  $json = '{
				  "type": "carousel",
				  "contents": [' . implode(",",$content) . ']
				}';
	
			$flexMessageBuilder = new RawMessageBuilder([
				'type' => 'flex',
				'altText' => ''.$key.' insta stories',
				'contents' => json_decode($json)
			]);
			return $bot->replyMessage($replyToken, $flexMessageBuilder);
		}
		
	}
	


	

}